<?php

$dadosGerais = [
    'linkCompra' => 'https://checkout.mycheckout.com.br/checkout/61c059c7ac99c37b9d8ccc2f?'
];

$dadosFooter = [
    'isFooterImage' => false,
    'footerClass'   => '',
    'footer'        => '',
    'hasText'       => false,
    'colsFooter'    => [
        'divider' => 'col-md-4',
        'middle'  => 'col-md-4'
    ]
];

?>

<!DOCTYPE html>
<html lang="pt-BR">

<head>
    <?php require('default/header.php'); ?>
    <title>PRINCIPIUM 🔥</title>
    <link rel="stylesheet" href="css/principium/cssUsed.css">
</head>

<body>
    <?php require('contents/principium_content.php'); ?>
    <?php require('contents/about/principium_about.php'); ?>
    <?php require('default/lastSectionV2.php'); ?>
    <?php require('default/scriptsBody.php'); ?>
</body>

<?php require('default/footer.php'); ?>

</html>