<?php 
    $data = [
        'neonBehavior'     => '',
        'textColor'        => '',
        'showButton'       => false,
        'button'           => '',
        'isFooterImage'    => '',
        'footerClass'      => '',
        'footer'           => '',
        'hasLabelGarantia' => false,
        'cols'             => []
    ];
?>


<div class="background-lastsection container-fluid">
    <?php 
        foreach ($dadosGerais as $key => $value) {
            foreach ($value as $key2 => $value2) {
                $data[$key2] = $value2;
            }
            require($key);
        }
    ?>
</div>