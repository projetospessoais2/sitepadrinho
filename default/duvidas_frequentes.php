<div class="row mt-3">
    <div class="col-md-3"></div>
    <div class="col-md-6">

        <div class="accordion" id="accordionExample">
            <div class="duvidas accordion-item">
                <h2 class="accordion-header bg-dark" id="headingOne">
                    <button class="duvidas accordion-button text-weight-bold f-size-0-7 neon-behavior-purple" type="button" data-bs-toggle="collapse" data-bs-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                        Em quanto tempo vou ter meu acesso?
                    </button>
                </h2>
                <div id="collapseOne" class="accordion-collapse collapse show" aria-labelledby="headingOne" data-bs-parent="#accordionExample">
                    <div class="duvidas accordion-body text-color-white text-weight-light f-size-1 neon-behavior-white-light">
                        Caso você adquira o livro pagando no cartão, o acesso é imediato. Vai chegar um e-mail pra você com o seu login e senha. Caso você adquira o livro pagando no boleto, o sistema pede 3 dias úteis para liberar o pagamento junto ao banco, e, assim que liberado, você recebe o e-mail de acesso.
                    </div>
                </div>
            </div>
            <div class="duvidas accordion-item">
                <h2 class="accordion-header" id="headingTwo">
                    <button class="duvidas accordion-button collapsed text-weight-bold f-size-0-7 neon-behavior-purple" type="button" data-bs-toggle="collapse" data-bs-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                        Como vou receber meu acesso?
                    </button>
                </h2>
                <!-- <hr class="duvidas"> -->
                <div id="collapseTwo" class="accordion-collapse collapse" aria-labelledby="headingTwo" data-bs-parent="#accordionExample">
                    <div class="duvidas accordion-body text-color-white text-weight-light f-size-1 neon-behavior-white-light">
                        Assim que o pagamento for processado pelo sistema da Evermart (plataforma onde você vai baixar o livro), você vai receber o acesso por e-mail.
                    </div>
                </div>
            </div>
            <div class="duvidas accordion-item">
                <h2 class="accordion-header" id="headingThree">
                    <button class="duvidas accordion-button collapsed text-weight-bold f-size-0-7 neon-behavior-purple" type="button" data-bs-toggle="collapse" data-bs-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                        Por quanto tempo vou poder acessar o livro?
                    </button>
                </h2>
                <div id="collapseThree" class="accordion-collapse collapse" aria-labelledby="headingThree" data-bs-parent="#accordionExample">
                    <div class="duvidas accordion-body text-color-white text-weight-light f-size-1 neon-behavior-white-light">
                        O acesso ao livro é vitalício, ou seja, uma vez que comprou, tem o sem acesso pra sempre.
                    </div>
                </div>
            </div>
            <div class="duvidas accordion-item">
                <h2 class="accordion-header" id="headingFour">
                    <button class="duvidas accordion-button collapsed text-weight-bold f-size-0-7 neon-behavior-purple" type="button" data-bs-toggle="collapse" data-bs-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                        Eu tenho alguma garantia?
                    </button>
                </h2>
                <div id="collapseFour" class="accordion-collapse collapse" aria-labelledby="headingFour" data-bs-parent="#accordionExample">
                    <div class="duvidas accordion-body text-color-white text-weight-light f-size-1 neon-behavior-white-light">
                        Sim! Caso você não goste do conteúdo, pode pedir o reembolso por dentro da própria plataforma dentro de um período de 7 dias após a compra.
                    </div>
                </div>
            </div>
            <div class="duvidas accordion-item">
                <h2 class="accordion-header" id="headingFive">
                    <button class="duvidas accordion-button collapsed text-weight-bold f-size-0-7 neon-behavior-purple" type="button" data-bs-toggle="collapse" data-bs-target="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
                        Tem uma ordem lógica dos livros?
                    </button>
                </h2>
                <div id="collapseFive" class="accordion-collapse collapse" aria-labelledby="headingFive" data-bs-parent="#accordionExample">
                    <div class="duvidas accordion-body text-color-white text-weight-light f-size-1 neon-behavior-white-light">Sim e não rsrsrs. Os livros não são interligados, mas foram escritos na seguinte ordem:
                        <br><br>
                        PRELIMINAR > CATIVAR > LIBERTAR > DESPERTAR
                    </div>
                </div>
            </div>
            <div class="duvidas accordion-item">
                <h2 class="accordion-header" id="headingSix">
                    <button class="duvidas accordion-button collapsed text-weight-bold f-size-0-7 neon-behavior-purple" type="button" data-bs-toggle="collapse" data-bs-target="#collapseSix" aria-expanded="false" aria-controls="collapseSix">
                        Como faço para tirar minhas dúvidas?
                    </button>
                </h2>
                <div id="collapseSix" class="accordion-collapse collapse" aria-labelledby="headingSix" data-bs-parent="#accordionExample">
                    <div class="duvidas accordion-body text-color-white text-weight-light f-size-1 neon-behavior-white-light">
                        Dentro da própria plataforma tem a área de membros. Lá você pode perguntar o que quiser a respeito do conteúdo. Caso a dúvida for sobre acesso ou pagamento, você pode falar pelo e-mail suportepadrinho@gmail.com
                    </div>
                </div>
            </div>
            <div class="duvidas accordion-item">
                <h2 class="accordion-header" id="headingSeven">
                    <button class="duvidas accordion-button collapsed text-weight-bold f-size-0-7 neon-behavior-purple" type="button" data-bs-toggle="collapse" data-bs-target="#collapseSeven" aria-expanded="false" aria-controls="collapseSeven">
                        Consigo acessar pelo celular?
                    </button>
                </h2>
                <div id="collapseSeven" class="accordion-collapse collapse" aria-labelledby="headingSeven" data-bs-parent="#accordionExample">
                    <div class="duvidas accordion-body text-color-white text-weight-light f-size-1 neon-behavior-white-light">
                        Sim! Tanto pelo computador, pelo tablet ou celular. Você pode acessar o conteúdo por onde e quando quiser.
                    </div>
                </div>
            </div>
        </div>

    </div>
    <div class="col-md-3"></div>
</div>