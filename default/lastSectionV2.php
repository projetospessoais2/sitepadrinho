<div class="background-default banner-about-1 h-100 container-fluid" id="navigate">
    <div class="row">
        <div class="col-md-2"></div>
        <div class="col-md-8 mt-5">
            <div class="card duvidas-v2">
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-1"></div>
                        <div class="col-md-10 text-center">
                            <div class="show-element">
                                <img class="card-img-top overflow duvidas" src="assets/principium/padrinho.webp" alt="Card image cap">
                            </div>
                            <div class="mt-17 show-element"></div>
                            <p class="text-red text-weight-bolder neon-behavior-red-intense f-size-2 mt-3">
                                Chegou a sua hora de DOMINAR ESSE PODER também!
                            </p>
                            <?php if (!empty($dadosGerais['htmlSubTitle'])): ?>
                            <p class="text-color-white f-size-1-2 letter-space-1-5">
                                <?= $dadosGerais['htmlSubTitle'] ?>
                            </p>
                            <?php endif; ?>
                            <p class="text-color-white f-size-1-9 text-weight-bolder">
                                Tenha acesso imediato ao Principium por apenas:
                            </p>
                            <div class="row">
                                <div class="col-md-1"></div>
                                <div class="col-md-10">
                                    <img src="assets/duvidas/valor1.webp" alt="" class="card-img" style="width: 100%; height: 100%">
                                </div>
                                <div class="col-md-1"></div>
                            </div>
                            <p class="text-color-white f-size-1-7 neon-behavior-white-intense">
                                Ou R$297 à vista.
                            </p>
                            <a class="" href = "<?= $dadosGerais['linkCompra'] ?>">
                                <button class="btn btn-lg col-10 mt-5 btn-principium btn-large text-color-white letter-space-1-5 f-size-1-7 text-weight-bold neon-behavior-white-intense">
                                    QUERO DOMINAR ESSE PODER
                                </button>
                            </a>

                        </div>
                        <div class="col-md-1"></div>
                    </div>
                    <br>
                </div>
            </div>
        </div>
        <div class="col-md-2"></div>
    </div>
    <br><br><br>
</div>

<div class="background-default banner-about-2 h-100 container-fluid">
    <div class="row">
        <div class="col-md-2"></div>
        <div class="col-md-8 text-center mt-5">
            <p class="text-color-white f-size-2-2">
                E o melhor de tudo é que… 
            </p>
            <p class="text-red text-weight-bolder neon-behavior-red-intense f-size-2-2">
                Você pode testar por 7 dias sem compromisso
            </p>
            <div class="row">
                <div class="col-md-4"></div>
                <div class="col-md-4">
                    <img src="assets/duvidas/garantia.webp" alt="" class="card-img selo-garantia-img">
                </div>
                <div class="col-md-4"></div>
            </div>
            <p class="text-color-white f-size-1-4 mt-5">
                Você pode entrar para essa nova turma do curso, assistir às aulas, começar a aplicar, e, se por algum motivo você decidir que não quer continuar, você tem até 7 dias para receber o seu dinheiro de volta.
            </p>
            <p class="text-color-white f-size-1-4">
            <span class="neon-behavior-white-intense text-weight-bolder">O seu risco é absolutamente ZERO!</span> Nada a perder, mas MUITO a ganhar...
            </p>
        </div>
        <div class="col-md-2"></div>
    </div>
    <br>
</div>

<div class="background-default banner-about-red h-100 container-fluid">
    <?php require('duvidasFrequentesV2.php') ?>
</div>