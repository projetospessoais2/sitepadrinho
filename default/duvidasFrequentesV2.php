<div class="row">
    <div class="col-md-12 text-center">
        <h1 class="neon-white f-size-2-3 pt-5 letter-space-1-5">
            Dúvidas Frequentes
        </h1>
    </div>
</div>

<div class="row mt-3">
    <div class="col-md-1"></div>
    <div class="col-md-10 mt-1">

        <div class="accordion-menu">
            <ul class="no-border">

                <li class="">
                    <input type="checkbox" checked>
                    <i class="arrow v2"></i>
                    <h2 class="text-color-white text-weight-bold neon-behavior-white-intense f-size-1 accordion">
                            Todos conseguem aprender mesmo sem nunca ter feito uma massagem?
                    </h2>
                    <p class="text-black text-weight-bold f-size-1 accordion">
                        SIM! O curso ensina a Massagem com vídeos detalhados e comentados, com o passo-a-passo e também com uma massagem SEM CORTES.
                    </p>
                </li>

                <li class="">
                    <input type="checkbox" checked>
                    <i class="arrow v2"></i>
                    <h2 class="text-color-white text-weight-bold neon-behavior-white-intense f-size-1 accordion">
                        Como recebo meu acesso do curso?
                    </h2>
                    <p class="text-black text-weight-bold f-size-1 accordion">
                        IMEDIATO! Você recebe em seu e-mail o acesso assim que o pagamento for confirmado pela plataforma.
                    </p>
                </li>

                <li class="">
                    <input type="checkbox" checked>
                    <i class="arrow v2"></i>
                    <h2 class="text-color-white text-weight-bold neon-behavior-white-intense f-size-1 accordion">
                        Funciona do mesmo jeito em todas as pessoas?
                    </h2>
                    <p class="text-black text-weight-bold f-size-1 accordion">
                        NÃO! Cada pessoa reage de um jeito dependendo do estímulo que recebe. Por isso, no curso você aprende também a identificar os sinais do prazer.
                    </p>
                </li>

                <li class="">
                    <input type="checkbox" checked>
                    <i class="arrow v2"></i>
                    <h2 class="text-color-white text-weight-bold neon-behavior-white-intense f-size-1 accordion">
                        E se eu não gostar?
                    </h2>
                    <p class="text-black text-weight-bold f-size-1 accordion">
                        A Lei, a Plataforma e o Padrinho te garantem 7 dias para assistir tudo e aplicar. Caso não goste, seu dinheiro é TOTALMENTE DEVOLVIDO com apenas 1 clique.
                    </p>
                </li>

                <li class="">
                    <input type="checkbox" checked>
                    <i class="arrow v2"></i>
                    <h2 class="text-color-white text-weight-bold neon-behavior-white-intense f-size-1 accordion">
                        E se eu tiver dúvidas em relação ao curso?
                    </h2>
                    <p class="text-black text-weight-bold f-size-1 accordion">
                        Você terá uma área de membros exclusiva para comentar todas as suas dúvidas para receber o suporte do Padrinho dentro da plataforma por 1 ano.
                    </p>
                </li>

                <li class="">
                    <input type="checkbox" checked>
                    <i class="arrow v2"></i>
                    <h2 class="text-color-white text-weight-bold neon-behavior-white-intense f-size-1 accordion">
                        Quanto tempo de acesso terei ao curso?
                    </h2>
                    <p class="text-black text-weight-bold f-size-1 accordion">
                        Durante 1 ano seu acesso está garantido para assistir quando e onde quiser.
                    </p>
                </li>
                
            </ul>
        </div>

    </div>
    <div class="col-md-1"></div>
</div>

<br><br>