<div class="background-lastsection container-fluid">
    <!-- TITULO -->
    <div class="row text-center">
        <div class="col-md-1"></div>
        <div class="col-md-10 mt-8">
            <h2 class="text-color-white f-size-2-5 text-weight-bold transform-upper">

                Quem é o <span>Padrinho?</span>

            </h2>
        </div>

        <div class="col-md-1"></div>
    </div>

    <!-- TITULO -->
    <div class="row">
        <div class="col-md-2"></div>
        <div class="col-md-4 mt-8 text-color-white">

            João Vitor Heringer, mais conhecido como Padrinho, ensina uma nova vertente sobre o Tantra e já ajudou milhares de pessoas com seu método. <br><br>

            Transformando, libertando e ensinando com amor através das redes sociais, ele traz uma perspectiva inédita sobre vários assuntos. <br><br>

            Teve seu primeiro contato com o Tantra em seu curso de Massoterapia, aos 20 anos. E por sentir uma verdade profunda ligada a sua infância, se interessou cada vez mais em aprender e ensinar. <br><br>

            Tendo se encontrado pelo propósito da libertação e sentindo o chamado para guiar outras Almas e restaurar relações através da filosofia do Tantra, ele hoje te convida a aprender sobre a arte de CATIVAR.
            <br>
            <span class="span-lastsection">
                (João Vitor Heringer, o Padrinho.)
            </span>

        </div>
        <div class="col-md-3 text-center">
            <img class="card-img" src="assets/cativar/padrinho_logo.webp" alt="Card image cap">
        </div>

        <div class="col-md-1"></div>
    </div>


    <div class="row mt-8">
        <div class="col-md-5"></div>
        <div class="col-md-2 text-center">
            <img class="card-img" src="assets/selo-garantia.webp" alt="Card image cap">
        </div>
        <div class="col-md-5"></div>
    </div>

    <div class="row mt-5">
        <div class="col-md-12 text-center">
            <h1 class="neon-white f-size-2-5 pt-5">
                Dúvidas Frequentes
            </h1>
        </div>
    </div>

    <?php require ('default/duvidas_frequentes.php'); ?>

    <br><br><br>
</div>