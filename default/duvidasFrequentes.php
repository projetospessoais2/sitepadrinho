<div class="row">
    <div class="col-md-5"></div>
    <div class="col-md-2 mt-8 text-center">
        <img class="card-img selo-garantia-img" src="assets/selo-garantia.webp" alt="Card image cap">
    </div>
    <div class="col-md-5"></div>
</div>

<?php if($data['hasLabelGarantia']): ?>

<div class="row">
    <div class="col-md-3"></div>
    <div class="col-md-6 mt-5 text-center text-color-white f-size-1-1 text-weight-bold">
        <p>
            Eu não estou te pedindo para você confiar em mim.
        </p>
        <p>
            Por isso você vai poder acessar o ENCANTAR durante 7 dias, sem compromisso comigo.
        </p>
        <p>
            Se por algum motivo você acreditar que ele não é para você.
        </p>
        <p>
            Eu devolvo o seu dinheiro de volta com apenas um clique.
        </p>
        <p>
            Sem ressentimentos e a amizade continua.
        </p>
    </div>
    <div class="col-md-3"></div>
</div>

<?php endif; ?>

<div class="mt-neg-15 show-element"></div>

<div class="row mt-5">
    <div class="col-md-12 text-center">
        <h1 class="neon-white f-size-2-5 pt-5">
            Dúvidas Frequentes
        </h1>
    </div>
</div>

<div class="row mt-3">
    <div class="<?= $data['cols']['divider'] ?>"></div>
    <div class="<?= $data['cols']['middle'] ?>">

        <div class="accordion-menu">
            <ul class="<?= $data['textColor'] ?>">
            
                <li class="<?= $data['textColor'] ?>">
                    <input type="checkbox" checked>
                    <i class="arrow <?= $data['textColor'] ?>"></i>
                    <h2 class="<?= $data['textColor'] ?> text-weight-bolder f-size-1 <?= $data['neonBehavior'] ?> accordion">
                        Em quanto tempo vou ter meu acesso?
                    </h2>
                    <p class="text-color-white text-weight-light f-size-0-8 neon-behavior-white-intense accordion">
                        Caso você adquira o livro pagando no cartão, o acesso é imediato. Vai chegar um e-mail pra você com o seu login e senha. Caso você adquira o livro pagando no boleto, o sistema pede 3 dias úteis para liberar o pagamento junto ao banco, e, assim que liberado, você recebe o e-mail de acesso.
                    </p>
                </li>

                <li class="<?= $data['textColor'] ?>">
                    <input type="checkbox" checked>
                    <i class="arrow <?= $data['textColor'] ?>"></i>
                    <h2 class="<?= $data['textColor'] ?> text-weight-bolder f-size-1 <?= $data['neonBehavior'] ?> accordion">
                        Como vou receber meu acesso?
                    </h2>
                    <p class="text-color-white text-weight-light f-size-0-8 neon-behavior-white-intense accordion">
                        Assim que o pagamento for processado pelo sistema da Evermart (plataforma onde você vai baixar o livro), você vai receber o acesso por e-mail.
                    </p>
                </li>

                <li class="<?= $data['textColor'] ?>">
                    <input type="checkbox" checked>
                    <i class="arrow <?= $data['textColor'] ?>"></i>
                    <h2 class="<?= $data['textColor'] ?> text-weight-bolder f-size-1 <?= $data['neonBehavior'] ?> accordion">
                        Por quanto tempo vou poder acessar o livro?
                    </h2>
                    <p class="text-color-white text-weight-light f-size-0-8 neon-behavior-white-intense accordion">
                        O acesso ao livro é vitalício, ou seja, uma vez que comprou, tem o sem acesso pra sempre.
                    </p>
                </li>

                <li class="<?= $data['textColor'] ?>">
                    <input type="checkbox" checked>
                    <i class="arrow <?= $data['textColor'] ?>"></i>
                    <h2 class="<?= $data['textColor'] ?> text-weight-bolder f-size-1 <?= $data['neonBehavior'] ?> accordion">
                        Eu tenho alguma garantia?
                    </h2>
                    <p class="text-color-white text-weight-light f-size-0-8 neon-behavior-white-intense accordion">
                        Sim! Caso você não goste do conteúdo, pode pedir o reembolso por dentro da própria plataforma dentro de um período de 7 dias após a compra.
                    </p>
                </li>

                <li class="<?= $data['textColor'] ?>">
                    <input type="checkbox" checked>
                    <i class="arrow <?= $data['textColor'] ?>"></i>
                    <h2 class="<?= $data['textColor'] ?> text-weight-bolder f-size-1 <?= $data['neonBehavior'] ?> accordion">
                        Tem uma ordem lógica dos livros?
                    </h2>
                    <p class="text-color-white text-weight-light f-size-0-8 neon-behavior-white-intense accordion">
                        Sim e não rsrsrs. Os livros não são interligados, mas foram escritos na seguinte ordem:
                        <br><br>
                        PRELIMINAR > CATIVAR > LIBERTAR > DESPERTAR
                    </p>
                </li>

                <li class="<?= $data['textColor'] ?>">
                    <input type="checkbox" checked>
                    <i class="arrow <?= $data['textColor'] ?>"></i>
                    <h2 class="<?= $data['textColor'] ?> text-weight-bolder f-size-1 <?= $data['neonBehavior'] ?> accordion">
                        Como faço para tirar minhas dúvidas?
                    </h2>
                    <p class="text-color-white text-weight-light f-size-0-8 neon-behavior-white-intense accordion">
                        Dentro da própria plataforma tem a área de membros. Lá você pode perguntar o que quiser a respeito do conteúdo. Caso a dúvida for sobre acesso ou pagamento, você pode falar pelo e-mail suportepadrinho@gmail.com
                    </p>
                </li>

                <li class="<?= $data['textColor'] ?>">
                    <input type="checkbox" checked>
                    <i class="arrow <?= $data['textColor'] ?>"></i>
                    <h2 class="<?= $data['textColor'] ?> text-weight-bolder f-size-1 <?= $data['neonBehavior'] ?> accordion">
                        Consigo acessar pelo celular?
                    </h2>
                    <p class="text-color-white text-weight-light f-size-0-8 neon-behavior-white-intense accordion">
                        Sim! Tanto pelo computador, pelo tablet ou celular. Você pode acessar o conteúdo por onde e quando quiser.
                    </p>
                </li>

            </ul>
        </div>

    </div>
    <div class="<?= $data['cols']['divider'] ?>"></div>
</div>

<?php
if ($data['showButton']) :
?>

    <div class="row">
        <div class="col-md-3"></div>
        <div class="col-md-6 mt-4 text-center">
            <a href="#navigate">
                <button class="<?= $data['button']['class'] ?>" id="final">
                    <?= $data['button']['text'] ?>
                </button>
            </a>

            <?php
            if (!empty($data['button']['subtitle'])) :
            ?>

                <div class="col-12 text-center">
                    <p class="text-color-white letter-space-1-5 lh-1-2 f-size-0-8 mt-1">por apenas 7x de R$ 6,35</p>
                </div>

            <?php endif; ?>

        </div>
        <div class="col-md-3"></div>
    </div>

<?php endif; ?>

<br><br>