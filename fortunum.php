<?php

header("Location: https://www.dicasdopadrinho.com/fortunum-vagas-abertas");

$dadosFooter = [
    'isFooterImage' => true,
    'footer'        => 'assets/fortunum/fortunum_logo.webp',
    'hasText'       => false,
    'colsFooter'    => [
        'divider' => 'col-md-5',
        'middle'  => 'col-md-2'
    ],
];

?>

<!DOCTYPE html>
<html lang="pt-BR">
    <head>
        <?php require ('default/header.php'); ?>
        <title>FORTUNUM</title>
        <link rel="stylesheet" href="css/fortunum/fortunum_style.css"> 
        <!-- Meta Pixel Code -->
            <script>
            !function(f,b,e,v,n,t,s)
            {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
            n.callMethod.apply(n,arguments):n.queue.push(arguments)};
            if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
            n.queue=[];t=b.createElement(e);t.async=!0;
            t.src=v;s=b.getElementsByTagName(e)[0];
            s.parentNode.insertBefore(t,s)}(window, document,'script',
            'https://connect.facebook.net/en_US/fbevents.js');
            fbq('init', '800660870812998');
            fbq('track', 'PageView');
            </script>
            <noscript><img height="1" width="1" style="display:none"
            src="https://www.facebook.com/tr?id=800660870812998&ev=PageView&noscript=1"
            /></noscript>
        <!-- End Meta Pixel Code -->
    </head>

    <body>
        <?php require ('contents/fortunum_content.php'); ?>
        <?php require('default/scriptsBody.php'); ?>
    </body>

    <?php require ('default/footer.php'); ?>
</html>