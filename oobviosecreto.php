<?php
    
$headLine    = "Você se acha feio?";
$subHeadLine = "Aprenda neste documentário, a se tornar uma pessoa interessante independentemente da sua aparência e entender como encantar e conquistar qualquer mulher.";
$classHeadl  = 'f-size-2-3';
$utmTerm     = '1';
$todaysDate  = date("Y-m-d H:i:s");

if (!empty($_GET['pag'])) {
    if (intval($_GET['pag']) == 2) {
        $headLine    = "ALGUMA MULHER JÁ TE AGRADECEU POR TER DORMIDO COM VOCÊ?";
        $subHeadLine = "Você aprenderá uma técnica que já ajudou milhares de homens de terem a sensação de fazer uma mulher agradecer no dia seguinte.";
        $classHeadl  = 'f-size-2';
        $utmTerm     = '2';
    } else if (intval($_GET['pag']) == 3) {
        $headLine    = "VOCÊ ACHA O SEU <span class='no-shadow'>🍆</span> PEQUENO OU TEM MEDO DELE NÃO SUBIR NA HORA H?";
        $subHeadLine = "Descubra como usar a energia adormecida em você e que vai te tornar uma pessoa mais poderosa na <span class='no-shadow'><img draggable='false' role='img' class='emoji' alt='🛏' src='https://s.w.org/images/core/emoji/13.1.0/svg/1f6cf.svg'></span>";
        $classHeadl  = 'f-size-1-6';
        $utmTerm     = '3';
    } else if (intval($_GET['pag']) == 4) {
        $headLine    = "NA HORA H, VOCÊ NÃO DURA MUITO?";
        $subHeadLine = "Como se livrar do maior inimigo de todo homem na cama e ter a confiança para dar uma noite mais do que perfeita para a sua parceira.";
        $classHeadl  = 'f-size-2-2';
        $utmTerm     = '4';
    } else if (intval($_GET['pag']) == 5) {
        $headLine    = "DOMINE O PODER DE CAUSAR O AUGE DO PRAZER";
        $subHeadLine = "Aprenda técnicas milenares que vão te fazer ser inesquecível, desejado, seguro e poderoso na <span class='text-weight-extrabold'>cama</span> e na <span class='text-weight-extrabold'>vida</span>.";
        $classHeadl  = 'f-size-2-6';
        $utmTerm     = '5';
    }
}

$dadosFooter = [
    'isFooterImage' => true,
    'footer'        => 'assets/obvioSecreto/obvioSecreto.webp',
    'hasText'       => false,
    'colsFooter'    => [
        'divider' => 'col-md-5',
        'middle'  => 'col-md-2'
    ],
];

?>

<!DOCTYPE html>
<html lang="pt-BR">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <meta name="viewport" content="initial-scale=1" />
        <!-- Google Tag Manager -->
        <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
        new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
        })(window,document,'script','dataLayer','GTM-MXC9HV6');</script>
        <!-- End Google Tag Manager -->
        <title>O ÓBVIO SECRETO</title>

        <link rel="icon" type="image/x-icon" href="./assets/favicon.ico">
        <link rel="stylesheet" href="./css/obvioSecreto/obvioSecreto.css">
    </head>
    <body>
        <!-- Google Tag Manager (noscript) -->
        <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-MXC9HV6"
        height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
        <!-- End Google Tag Manager (noscript) -->
        <?php require ('contents/oobviosecreto_content.php'); ?>
    </body>

    <script type="text/javascript">
        window.cfields = {"3":"utm_source","4":"utm_medium","6":"utm_campaign","8":"utm_term"};

        window._show_thank_you = function(id, message, trackcmp_url, email) {
            document.querySelector('#_form_1_submit').disabled = false;
            window.location.href = "https://www.dicasdopadrinho.com/parabens"
            // var form = document.getElementById('_form_' + id + '_'), thank_you = form.querySelector('._form-thank-you');
            // form.querySelector('._form-content').style.display = 'none';
            // thank_you.innerHTML = message;
            // thank_you.style.display = 'block';
            // const vgoAlias = typeof visitorGlobalObjectAlias === 'undefined' ? 'vgo' : visitorGlobalObjectAlias;
            // var visitorObject = window[vgoAlias];
            // if (email && typeof visitorObject !== 'undefined') {
            //     visitorObject('setEmail', email);
            //     visitorObject('update');
            // } else if (typeof(trackcmp_url) != 'undefined' && trackcmp_url) {
            //     // Site tracking URL to use after inline form submission.
            //     _load_script(trackcmp_url);
            // }
            // if (typeof window._form_callback !== 'undefined') window._form_callback(id);
        };

        // window._show_error = function(id, message, html) {
        //     var form = document.getElementById('_form_' + id + '_'), err = document.createElement('div'), button = form.querySelector('button'), old_error = form.querySelector('._form_error');
        //     if (old_error) old_error.parentNode.removeChild(old_error);
        //     err.innerHTML = message;
        //     err.className = '_error-inner _form_error _no_arrow';
        //     var wrapper = document.createElement('div');
        //     wrapper.className = '_form-inner';
        //     wrapper.appendChild(err);
        //     button.parentNode.insertBefore(wrapper, button);
        //     document.querySelector('[id^="_form"][id$="_submit"]').disabled = false;
        //     if (html) {
        //         var div = document.createElement('div');
        //         div.className = '_error-html';
        //         div.innerHTML = html;
        //         err.appendChild(div);
        //     }
        // };

        window._load_script = function(url, callback) {
            var head = document.querySelector('head'), script = document.createElement('script'), r = false;
            script.type = 'text/javascript';
            script.charset = 'utf-8';
            script.src = url;
            if (callback) {
                script.onload = script.onreadystatechange = function() {
                if (!r && (!this.readyState || this.readyState == 'complete')) {
                    r = true;
                    callback();
                }
                };
            }
            head.appendChild(script);
        };

        (function() {

            if (window.location.search.search("excludeform") !== -1) return false;
            var getCookie = function(name) {
                var match = document.cookie.match(new RegExp('(^|; )' + name + '=([^;]+)'));
                return match ? match[2] : null;
            }
            var setCookie = function(name, value) {
                var now = new Date();
                var time = now.getTime();
                var expireTime = time + 1000 * 60 * 60 * 24 * 365;
                now.setTime(expireTime);
                document.cookie = name + '=' + value + '; expires=' + now + ';path=/';
            }
            
            var addEvent = function(element, event, func) {
                if (element.addEventListener) {
                element.addEventListener(event, func);
                } else {
                var oldFunc = element['on' + event];
                element['on' + event] = function() {
                    oldFunc.apply(this, arguments);
                    func.apply(this, arguments);
                };
                }
            }

            var form_to_submit = document.getElementById('_form_1_');
            var allInputs = form_to_submit.querySelectorAll('input, select, textarea'), tooltips = [], submitted = false;

            var getUrlParam = function(name) {
                var params = new URLSearchParams(window.location.search);
                return params.get(name) || false;
            };

            for (var i = 0; i < allInputs.length; i++) {
                var regexStr = "field\\[(\\d+)\\]";
                var results = new RegExp(regexStr).exec(allInputs[i].name);
                var fieldVal

                if (results != undefined) {
                    fieldVal = getUrlParam(window.cfields[results[1]]);
                } else {
                    fieldVal = false
                }

                if (fieldVal) {
                    allInputs[i].value = fieldVal;
                }
            }
        
            var _form_serialize = function(form){if(!form||form.nodeName!=="FORM"){return }var i,j,q=[];for(i=0;i<form.elements.length;i++){if(form.elements[i].name===""){continue}switch(form.elements[i].nodeName){case"INPUT":switch(form.elements[i].type){case"text":case"number":case"date":case"time":case"hidden":case"password":case"button":case"reset":case"submit":q.push(form.elements[i].name+"="+encodeURIComponent(form.elements[i].value));break;case"checkbox":case"radio":if(form.elements[i].checked){q.push(form.elements[i].name+"="+encodeURIComponent(form.elements[i].value))}break;case"file":break}break;case"TEXTAREA":q.push(form.elements[i].name+"="+encodeURIComponent(form.elements[i].value));break;case"SELECT":switch(form.elements[i].type){case"select-one":q.push(form.elements[i].name+"="+encodeURIComponent(form.elements[i].value));break;case"select-multiple":for(j=0;j<form.elements[i].options.length;j++){if(form.elements[i].options[j].selected){q.push(form.elements[i].name+"="+encodeURIComponent(form.elements[i].options[j].value))}}break}break;case"BUTTON":switch(form.elements[i].type){case"reset":case"submit":case"button":q.push(form.elements[i].name+"="+encodeURIComponent(form.elements[i].value));break}break}}return q.join("&")};

            var form_submit = function(e) {
                e.preventDefault();
                document.querySelector('#_form_1_submit').disabled = true;
                var serialized = _form_serialize(document.getElementById('_form_1_')).replace(/%0A/g, '\\n');
                var err = form_to_submit.querySelector('._form_error');
                err ? err.parentNode.removeChild(err) : false;
                _load_script('https://dicasdopadrinho.activehosted.com/proc.php?' + serialized + '&jsonp=true');
                return false;
            };
            
            addEvent(form_to_submit, 'submit', form_submit);
        })();

    </script>

    <?php require ('default/footer.php'); ?>
</html>