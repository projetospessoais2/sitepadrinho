<?php

$dadosGerais = [
    'contents/about/encantar_about.php' => [],
    'default/duvidasFrequentes.php' => [
        'textColor'        => 'encantar',
        'neonBehavior'     => 'neon-behavior-blue-intense',
        'hasLabelGarantia' => true,
        'cols'             => [
            'divider' => 'col-md-1',
            'middle'  => 'col-md-10'
        ]
    ],
];

$dadosFooter = [
    'isFooterImage' => true,
    'footerClass'   => '',
    'footer'        => 'assets/encantar/encantar_logo.webp',
    'hasText'       => false,
    'colsFooter'    => [
        'divider' => 'col-md-5',
        'middle'  => 'col-md-2'
    ]
];

?>

<!DOCTYPE html>
<html lang="pt-BR">

<head>
    <?php require('default/header.php'); ?>
    <title>ENCANTAR</title>
    <link rel="stylesheet" href="css/encantar/encantar.css">
</head>

<body>
    <?php require('contents/encantar_content.php'); ?>
    <?php require('default/loadLastSection.php'); ?>
    <?php require('default/scriptsBody.php'); ?>
</body>

<?php require('default/footer.php'); ?>

</html>