<div class="background-default banner1 container-fluid">
    
    <div class="mt-neg-12 show-element"></div>
    <!-- TITULO -->
    <div class="row text-center">
        <h1 class="neon-white shadow-pink mt-8 titleCativar">CATIVAR</h1>
    </div>

    <div class="mt-neg-12 show-element"></div>
    <!-- TEXTO INICIAL E VIDEO (CONTAINER 1) -->
    <div class="row mt-8">
        <div class="col-md-1 resize-bs-col-md-1-upper"></div>
        <div class="col-md-5 resize-bs-col-md-5-lower" id="divClassButtonInitial">
            <h2 class="text-color-white f-size-2-3 text-weight-extrabold lh-1 transform-upper">E se <span>você</span> pudesse se tornar alguém leve, cativante e <span>realmente</span> marcante?</h2>
            <p class="text-color-white letter-space-1-5 lh-1-2 f-size-1-3 size1 mt-5 work-sans-font text-weight-lighter">
                Com a arte de <span>CATIVAR</span>, a vida se eleva ao patamar de <span>poder receber</span> o que você já sabe que merece, sem se preocupar em perder anos correndo atrás de <span>algo, ou alguém...</span>
            </p>
        </div>

        <div class="col-md-4 text-center mt-5">

            <div class="mt-neg-12 show-element"></div>
            <div class="ratio ratio-16x9">

                <lite-youtube videoid="WwtEskhSrII" style="background-image: url('https://i.ytimg.com/vi/WwtEskhSrII/hqdefault.jpg');">
                    <a href="https://youtube.com/watch?v=WwtEskhSrII" class="lty-playbtn" title="Play Video">
                        <span class="lyt-visually-hidden">Play Video: Keynote (Google I/O '18)</span>
                    </a>
                </lite-youtube>
                
            </div>
        </div>

        <div class="col-md-1"></div>
    </div>

    <div class="row">
        <div class="col-md-1 resize-bs-col-md-1-upper"></div>

        <div class="col-md-6 resize-bs-col-md-6-lower">
            <div id="divClassButtonInitial">
                <a class="" href="#navigate">
                    <button class="btn btn-lg col-10 mt-3 btn-aprender text-color-white letter-space-1-5 lh-1-2 f-size-1-4 text-weight-bold">
                        QUERO APRENDER A CATIVAR
                    </button>
                </a>
            </div>
            <div class="col-md-10 text-center">
                <p class="text-color-white letter-space-1-5 lh-1-2 f-size-1 mt-1">por apenas 7x de R$ 6,35</p>
            </div>
        </div>


        <div class="col-md-5" id="resize-bs-col-md-5-upper"></div>

    </div>


    <div class="mt-neg-20 show-element"></div>
    <!-- TEXTO SOBRE O LIVRO (CONTAINER 2) -->
    <div class="row mt-10 text-center">
        <div class="col-md-2"></div>
        <div class="col-md-8 text-color-white letter-space-1-5 lh-1-2 f-size-1-2">

            <div class="convencimento-default f-size-1-3 lh-1-2 work-sans-font text-weight-light">
                Sendo <span>homem ou mulher,</span> agora você pode entender melhor sobre os <span>fluxos de
                    energias</span> e <span>comportamentos</span> que podem <span>melhorar</span> sua relação com <span>todos</span>,
                com o <span>trabalho</span>, com a <span>família</span> e também na <span>vida pessoal</span>.
            </div>

            <div class="mt-5 text-weight-extrabold lh-1-2 f-size-1-6">
                Talvez você esteja curioso, pensando:
            </div>

            <div class="mt-3 work-sans-font text-weight-light">
                <i>
                    Ok, mas qual seria o fundamento para me tornar uma pessoa cativante?
                </i>
            </div>

            <div class="mt-5 text-weight-extrabold remove-lh f-size-1-6">
                E a resposta é fácil:
                <div class="mt-4">
                    <span class="text-weight-extrabold">
                        O Tantra!
                    </span>
                </div>
            </div>

            <div class="mt-5 work-sans-font text-weight-light f-size-1-3 lh-1-2">
                Adaptando essa ideia <span class=" text-weight-bolder">milenar</span> que é o Tantra de uma maneira <span class=" text-weight-bolder">atual, fácil e aplicável,</span> o livro <span class=" text-weight-bolder">CATIVAR</span> te ensina sobre conexões reais em <span class=" text-weight-bolder">todos os âmbitos</span> da vida, compreendendo <span class=" text-weight-bolder">melhor</span> as pessoas e sendo <span class=" text-weight-bolder">cativante, em instantes!</span>
            </div>

            <div class="mt-4 work-sans-font text-weight-light f-size-1-3 lh-1-2">
                <span class=" text-weight-bolder">CATIVAR</span> é sobre saber trazer, <span class=" text-weight-bolder">receber,</span> lidar e manter MUITO MELHOR
                tudo que realmente desejamos e merecemos!
            </div>

            <div class="mt-5 text-weight-bold lh-1 f-size-4 work-sans-font">
                Com o livro <span class="destaque text-weight-extrabold">CATIVAR</span> você vai dominar essa arte <span class="destaque text-weight-extrabold">poderosa</span> de um modo que <span class="destaque text-weight-extrabold">ninguém</span> ousou te contar, ainda...
            </div>

            <div class="mt-neg-12 show-element"></div>

            <div class="mt-10 text-weight-extrabold lh-1 f-size-2">
                Sentiu <span class="destaque text-weight-extrabold">curiosidade?</span>
            </div>

            <div class="mt-3 convencimento-default text-weight-lighter f-size-1-4">
                Olha com carinho o que diz quem já leu...
            </div>
        </div>
        <div class="col-md-2"></div>
    </div>
</div>

<div class="background-default banner2 container-fluid">
    <br><br>

    <div class="row mt-3 text-center text-color-white letter-space-1-5 lh-1-2 f-size-1-2">
        <div class="col-md-4"></div>
        <div class="col-md-2">
            <div class="card bg-transparent">
                <img class="card-img-top" src="assets/cativar/feedback-cativar-1.webp" style="width: 100%; height: 100%">
            </div>
        </div>
        <div class="col-md-2">
            <div class="card bg-transparent">
                <img class="card-img-top" src="assets/cativar/feedback-cativar-2.webp" style="width: 100%; height: 100%">
            </div>
        </div>
        <div class="col-md-4"></div>

        <div class="mt-6 text-weight-extrabold lh-1 f-size-2">
            Tá gostando dessa <span class="destaque">ideia?</span>
        </div>

        <div class="mt-3 convencimento-default text-weight-lighter f-size-1-4">
            Então sente um pouco mais...
        </div>
    </div>

    <div class="row mt-6 text-center text-color-white letter-space-1-5 lh-1-2 f-size-1-2">
        <div class="col-md-4"></div>
        <div class="col-md-2">
            <div class="card bg-transparent">
                <img class="card-img-top" src="assets/cativar/feedback-cativar-3.webp" style="width: 100%; height: 100%">
            </div>
        </div>
        <div class="col-md-2">
            <div class="card bg-transparent">
                <img class="card-img-top" src="assets/cativar/feedback-cativar-4.webp" style="width: 100%; height: 100%">
            </div>
        </div>
        <div class="col-md-4"></div>

        <div class="row text-center">
            <div class="col-md-4"></div>
            <div class="col-md-4">
                <a class="" href="#navigate">
                    <button class="btn btn-lg col-12 mt-5 btn-aprender text-color-white letter-space-1-5 lh-1-2 f-size-1-2 text-weight-bold">
                        QUERO APRENDER A CATIVAR
                    </button>
                </a>
                <div class="col-12">
                    <p class="text-color-white letter-space-1-5 lh-1-2 f-size-0-8 description mt-1 work-sans-font text-weight-light">por apenas 7x de R$ 6,35</p>
                </div>
            </div>
            <div class="col-md-4"></div>
        </div>

    </div>

</div>


<div class="background-default banner1 container-fluid">
    <br><br>

    <div class="row mt-6 text-center text-color-white letter-space-1-5 lh-1-2 f-size-1-2">
        <div class="row mt-10 text-center">

            <div class="col-md-3"></div>
            <div class="col-md-6 text-color-white letter-space-1-5 lh-1-2 f-size-1-2">

                <div class="mt-neg-50 show-element"></div>
                <div class="text-weight-extrabold lh-1 f-size-2">
                    <span class="destaque text-weight-extrabold">Sentiu?</span> Então você já pode conferir
                    o que te espera no <span class="destaque text-weight-extrabold">CATIVAR:</span>
                </div>
            </div>
            <div class="col-md-3"></div>
        </div>

        <div class="row mt-4 text-center">
            <div class="col-md-2"></div>
            <div class="col-md-8">
                <div class="card bg-transparent">
                    <img class="card-img-top" src="assets/cativar/apresentacao_cativar.webp" style="width: 100%; height: 100%">
                </div>
            </div>
            <div class="col-md-2"></div>
        </div>
    </div>
</div>

<div class="background-default banner3 container-fluid" id="navigate">
    <!-- TITULO -->
    <div class="row text-center">
        <h1 class="neon-white shadow-pink mt-8 titleCativar hide-element">CATIVAR</h1>
    </div>

    <div class="row text-center">
        <div class="row text-center text-color-white letter-space-1-5 lh-1-2 f-size-1-2">

            <div class="col-md-1"></div>
            <div class="col-md-10 mt-6 text-color-white letter-space-1-5 lh-1-2 f-size-1-2">

                <div class="text-weight-extrabold f-size-2 lh-1-2">
                    Calmaaaaa que ainda tem mais...
                </div>

                <div class="mt-3 f-size-1-2 lh-1-2">
                    Veja tudo que você terá acesso levando o <span>CATIVAR</span> ainda hoje:
                </div>
            </div>
            <div class="col-md-1"></div>
        </div>

        <div class="row mt-5">

            <div class="col-md-3"></div>
            <div class="col-md-6">

                <div class="card mostra-precos">
                    <div class="card-body">
                        <!-- ROW LIVRO CATIVARS -->
                        <div class="row reverse">
                            <div class="col-md-3">
                                <img class="card-img mobile-img-1" src="assets/cativar/capa_cativar.webp">
                            </div>
                            <div class="col-md-9 mt-4 text-justify f-size-1-3 text-weight-extrabold">
                                LIVRO CATIVAR: A ARTE QUE NINGUÉM OUSOU TE CONTAR!
                            </div>
                        </div>

                        <!-- ROW BONUS 1 -->
                        <div class="row mt-3 reverse">
                            <div class="col-md-4">
                                <img class="card-img" src="assets/cativar/chocolatinho.webp" style="width: 100%; height: 100%">
                            </div>
                            <div class="col-md-8 mt-4 text-justify f-size-1-3 text-weight-extrabold">
                                BÔNUS #1: A tão esperada aula do CHOCOLATINHO
                            </div>
                        </div>

                        <!-- ROW TEXTOS APOS IMAGENS -->
                        <div class="row mt-4">
                            <div class="col-md-12">
                                <div class="f-size-2-2 text-weight-extrabold">
                                    Valor Total: <span class="text-weight-extrabold"><s>R$49,90</s></span>
                                </div>
                                <div class="text-black work-sans-font text-weight-lighter f-size-1-2">
                                    Mas você não vai pagar esse valor hoje… <br>
                                    Porque tudo isso vai ser seu por apenas
                                </div>
                                <div class="text-weight-extrabold roxo-cativar f-size-6 mt-2">
                                    7x de R$ 6,35
                                </div>
                                <div class="text-black text-weight-light f-size-4">
                                    ou <b>R$39,69</b> à vista
                                </div>
                                <a href="https://checkout.mycheckout.com.br/checkout/6074470743cd202c90a1e612?">
                                    <button class="btn btn-lg col-8 mt-4 btn-aprender maior text-weight-bold text-color-white f-size-1-6">
                                        QUERO COMPRAR AGORA
                                    </button>
                                </a>
                                <div class="text-black mt-1">
                                    🔒 Seus dados estão seguros
                                </div>

                            </div>

                        </div>

                    </div>
                </div>

            </div>
            <div class="col-md-3"></div>
        </div>

        <div class="row text-center mt-3">

            <div class="col-md-3"></div>
            <div class="col-md-3">
                <div class="card text-center bg-transparent">
                    <div class="card-header">
                        <a class="roxo-cativar f-size-6"><i class="fa-solid fa-star"></i></a>
                    </div>
                    <div class="card-body">
                        <span class="f-size-4">Pagamento Seguro</span>
                        <p class="text-color-white mt-2 f-size-1 font-weight-light">
                            Ambiente seguro. Seus dados estão
                            protegidos e sua compra é 100% segura.
                        </p>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="card text-center bg-transparent">
                    <div class="card-header">
                        <a class="roxo-cativar f-size-6"><i class="fa-solid fa-star"></i></a>
                    </div>
                    <div class="card-body">
                        <span class="f-size-4">Acesso Imediato</span>
                        <p class="text-color-white mt-2 f-size-1 font-weight-light">
                            Seu login e senha para acessar o produto serão enviados
                            ao seu e-mail logo após o processamento do pagamento.
                        </p>
                    </div>
                </div>
            </div>
            <div class="col-md-3"></div>
        </div>

    </div>
</div>

<div class="background-default banner1 container-fluid">
    <div class="row text-center">

        <div class="col-md-3"></div>
        <div class="col-md-6 mt-8">

            <div class="mt-neg-20 show-element"></div>
            <div class="text-color-white f-size-2-2 text-weight-extrabold">
                Agora você tem 3 opções:
            </div>

            <div class="text-color-white f-size-1-3 text-weight-light mt-5 letter-space-1-5 work-sans-font">
                1. Não dar o passo adiante e continuar seguindo a mesma vida que tem agora.
            </div>

            <div class="text-color-white f-size-1-3 text-weight-light mt-5 letter-space-1-5 work-sans-font">
                2. Tentar aprender por si só e correr o risco de perder tempo e dinheiro, e ainda sim, continuar se sentindo inseguro(a) e desinteressante.
            </div>

            <div class="text-color-white f-size-1-3 text-weight-light mt-5 letter-space-1-5 work-sans-font">
                3. Tomar a decisão de ser uma pessoa extremamente atraente e cativante com a ajuda do Padrinho.
            </div>

        </div>
        <div class="col-md-3"></div>
    </div>

    <br><br><br><br>
</div>