<div class="background-default banner1 container-fluid">
    
    <!-- TITULO  -->
    <div class="row justify-items-center">
        <img id="logo" class="img-responsive mt-3" src="assets/preliminar/logo-preliminar.webp" style="width: 100%; height: 100%">
    </div>

    <!-- TEXTO INICIAL E VIDEO (CONTAINER 1) -->
    <div class="mt-neg-12 show-element"></div>
    <div class="row mt-8">
        <div class="col-md-1 resize-bs-col-md-1-upper"></div>
        <div class="col-md-5 resize-bs-col-md-5-lower" id="divClassButtonInitial">
            <h2 class="text-color-white f-size-2-5 text-weight-extrabold">QUAL SEU CONCEITO DE <span>INTIMIDADE E ENERGIA?</span></h2>
            <p class="text-color-white letter-space-1-5 lh-1-2 f-size-1-4 mt-4 work-sans-font text-weight-lighter">Descubra os <span class="text-weight-bolder">segredos</span> do Tantra com esse novo conceito de <span class="text-weight-bolder">PRELIMINAR</span> e transforme sua <span class="text-weight-bolder">vida</span>, sua energia e sua <span class="text-weight-bolder">intimidade</span> de maneiras inimagináveis!</p>
        </div>
        <!-- <div class="col-md-1 resize-bs-col-md-1-upper"></div> -->
        <div class="col-md-4 text-center mt-3">
            <div class="ratio ratio-16x9">

                <lite-youtube videoid="32yHAXdRaWk" style="background-image: url('https://i.ytimg.com/vi/32yHAXdRaWk/hqdefault.jpg');">
                    <a href="https://youtube.com/watch?v=32yHAXdRaWk" class="lty-playbtn" title="Play Video">
                        <span class="lyt-visually-hidden">Play Video: Keynote (Google I/O '18)</span>
                    </a>
                </lite-youtube>
            </div>
        </div>
        <div class="col-md-1"></div>
    </div>

    <div class="row">
        <div class="col-md-1 resize-bs-col-md-1-upper"></div>
        <div class="col-md-5 text-center resize-bs-col-md-5-lower" id="divClassButtonInitial">
            <a class="" href="#navigate">
                <button class="btn btn-lg col-8 mt-4 btn-aprender text-color-white letter-space-1-5 lh-1-2 f-size-1-2 text-weight-bolder">QUERO APRENDER AGORA</button>
            </a>
        </div>
        <div class="col-md-6"></div>
    </div>

    <div class="mt-neg-12 show-element"></div>
    <!-- TEXTO SOBRE O LIVRO (CONTAINER 2) -->
    <div class="mt-10">
        <div class="row">
            <div class="col-md-1 resize-bs-col-md-1-upper"></div>
            <div class="col-md-5 resize-bs-col-md-5-lower" id="divClassButtonInitial">
                <h2 class="text-color-white f-size-2-5 text-weight-extrabold">Dentro do <span>PRELIMINAR</span> você irá descobrir:</h2>
                <p class="text-color-white letter-space-1-5 lh-1-2 f-size-1-2 mt-4 work-sans-font text-weight-lighter">
                    O que é, de fato, <span class="text-weight-bolder">VIVER e SENTIR prazer</span>
                    <br><br><br>
                    como conduzir melhor a energia do <span class="text-weight-bolder">Tantra,</span> consigo e com os outros que convive, se tornando aquela pessoa mais <span class="text-weight-bolder">enérgica,</span> leve, alegre e realizada na <span class="text-weight-bolder">vida e na intimidade!</span>
                    <br><br><br>
                    Desde a <span class="text-weight-bolder">Teoria do Tantra</span>, até dicas práticas sobre como proporcionar e sentir o verdadeiro <span class="text-weight-bolder">prazer VISCERAL!</span>
            </p>
            </div>
            <div class="col-md-6">
                <img src="assets/preliminar/livro-preliminar.webp" class="card-img book-img-preliminar" style="width: 100%; height: 100%">
            </div>
        </div>
    </div>    

</div>

    <!-- FEEDBACK -->
<div class="container-fluid background-default banner1">
        <div class="row text-center">
            <img id="logo" src="assets/preliminar/logo-preliminar.webp" class="mt-3">
        </div>
        <div class="row text-center mt-5">
            <h2 class="text-color-white">E talvez possa se perguntar:</h2>
        </div>
        <div class="row text-center">
            <h2 class="text-color-white text-weight-extrabold lh-1-2 f-size-2-2">"Será que um livro pode fazer tanta diferença assim?"</h2>
        </div>
        <div class="row text-center mt-4">
            <h3 class="text-color-white letter-space-1-5 lh-1-2 f-size-1-2">
                Olha, o Padrinho não costuma prometer nada,<br> mas olha com carinho o que quem já leu têm falado:</h3>
        </div>

    <div class="row mt-4">
        <div class="col-md-2"></div>
        <div class="col-md-3">
            <div class="card bg-transparent"><img src="assets/preliminar/feedback-preliminar-1.webp"  style="width: 100%; height: 100%"></div>
        </div>
        <div class="col-md-3">
            <div class="card bg-transparent"><img src="assets/preliminar/feedback-preliminar-2.webp"  style="width: 100%; height: 100%"></div>
        </div>
        <div class="col-md-3">
            <div class="card bg-transparent"><img src="assets/preliminar/feedback-preliminar-3.webp"  style="width: 100%; height: 100%"></div>
        </div>
        <div class="col-md-2"></div>
    </div>
    
    <div class="row mt-3 text-center">
        <div class="row text-center mt-4">
            <h2 class="text-color-white text-weight-extrabold lh-1-2 f-size-2-2">Entenda de maneira fácil como conduzir a sua própria<br>energia também com a de quem convive.</h2>
        </div>
        <div class="row text-center mt-4">
            <h3 class="text-color-white letter-space-1-5 lh-1-2 f-size-1-2">O livro PRELIMINAR te ensina também sobre o equilíbrio<br>e a diferença entre os Sagrados Masculino e do Feminino.</h3>
        </div>
        
        <div class="col-md-2"></div>
        <div class="col-md-4 mt-4">
            <div class="card bg-transparent"><img src="assets/preliminar/feedback-preliminar-1.1.webp" style="width: 100%; height: 100%"></div>
        </div>
        <div class="col-md-4 mt-4">   
            <div class="card bg-transparent"><img src="assets/preliminar/feedback-preliminar-1.2.webp" style="width: 100%; height: 100%"></div>
        </div>
        <div class="col-md-2"></div>
    </div>

        <div class="row text-center mt-5">
            <h2 class="text-color-white text-weight-extrabold">O prazer verdadeiro nunca começou entre quatro paredes!</h2>
        </div>
        <div class="row text-center">
            <h3 class="text-color-white letter-space-1-5 lh-1-2 f-size-1-3">Com o PRELIMINAR você aprende a viver, sentir e proporcionar<br>prazer de maneira leve, constante e inesquecível!</h3>
        </div>

    <!-- SÚMARIO -->

        <div class="mt-neg-12 show-element"></div>
        <div class="row text-center mt-10">
            <h2 class="text-color-white f-size-2-5 text-weight-extrabold">Quer ver o que te espera dentro do <span>PRELIMINAR</span></h2>
        </div>
        <div class="row text-center">
            <h3 class="text-color-white letter-space-1-5 lh-1-2 f-size-1-2">Então vem:</h3>
        </div>

        <div class="row text-center mt-4">
            <div class="col-md-2"></div>
                <div class="col-md-8">
                    <div class="card bg-transparent">
                        <img class="card-img-top" src="assets/preliminar/sumario-preliminar.webp" style="width: 100%; height: 100%">
                    </div>
                </div>
            <div class="col-md-2"></div>
        </div>

    <!-- CARD VENDA -->
        <div id="div-1">
        <div class="row text-center mt-6 md-4">
            <img id="logo" src="assets/preliminar/logo-preliminar.webp" class="hide-element">
        </div>
        <div class="row text-center mt-4">
            <h2 class="text-color-white f-size-2-5 responsive-smaller text-weight-extrabold">Dá uma olhada em tudo que você terá acesso <br> levando o <span>PRELIMNAR</span> ainda hoje:</h2>
        </div>
        <div class="text-center"> 
        <div class="row mt-5">       
        <div class="col-md-3"></div>
        <div class="col-md-6">
            <div class="card mostra-precos" id="navigate">
                <div class="card-body">

                        <div class="row text-center">
                            <div class="col-md-2"></div>
                            <div class="col-md-8 mt-4 f-size-2 text-weight-extrabold">
                                LIVRO PRELIMINAR
                            </div>
                            <div class="col-md-2"></div>
                        </div>

                        <!-- ROW LIVRO PRELIMINAR -->
                        <div class="row">
                            <div class="col-md-4">
                            </div>
                            <div class="col-md-4 mt-4 text-justify f-size-1-3 text-weight-extrabold">
                                <img class="card-img mobile-img-1" src="assets/preliminar/card-img-1.webp" alt="Card image cap">
                            </div>
                            <div class="col-md-4">
                        </div>
    
                        <!-- ROW BONUS 1 -->
                        <div class="row mt-5 reverse">
                            <div class="col-md-4">
                                <img class="card-img mobile-img-2" src="assets/preliminar/card-img-2.webp" alt="Card image cap">
                            </div>
                            <div class="col-md-8 mt-4 text-justify f-size-1-3 text-weight-extrabold">
                                BÔNUS #1: A tão esperada aula do CHOCOLATINHO
                            </div>
                        </div>
    
                        <!-- ROW TEXTOS APOS IMAGENS -->
                        <div class="row mt-4">
                            <div class="col-md-12">
                                <div class="f-size-2-2 text-weight-extrabold">
                                    Valor Total: <span><s>R$69,90</s></span>
                                </div>
                                <div class="text-black text-weight-light work-sans-font text-weight-lighter f-size-1-2">
                                    Mas você não vai pagar esse valor hoje… <br>
                                    Porque tudo isso vai ser seu por apenas
                                </div>
                                <div class="text-weight-extrabold roxo-cativar f-size-6 mt-2">
                                    <span>7x de R$ 6,35</span>
                                </div>
                                <div class="text-black text-weight-light f-size-4">
                                    ou <b>R$39,69</b> à vista
                                </div>
                                <a href="https://checkout.mycheckout.com.br/checkout/5ff87a94634e121e0a808c7d?">
                                    <button class="btn btn-lg col-10 mt-4 btn-aprender maior text-weight-bold text-color-white f-size-1-7">
                                        QUERO COMPRAR AGORA
                                    </button>
                                </a>
                                <div class="text-black mt-1">
                                    🔒 Seus dados estão seguros
                                </div>
    
                            </div>
    
                        </div>
    
                    </div>
                </div>
    
            </div>
        </div>
        <div class="col-md-3"></div>
                <div class="row text-center mt-3">

                    <div class="col-md-2"></div>
                    <div class="col-md-4">
                        <div class="card text-center bg-transparent">
                            <div class="card-header">
                                <a class="text-red-preliminar-v2 f-size-6"><i class="fa-solid fa-star"></i></a>
                            </div>
                            <div class="card-body">
                                <span class="f-size-4">Pagamento Seguro</span>
                                <p class="text-color-white mt-2 f-size-1 font-weight-light">
                                    Ambiente seguro. Seus dados estão
                                    protegidos e sua compra é 100% segura.
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="card text-center bg-transparent">
                            <div class="card-header">
                                <a class="text-red-preliminar-v2 f-size-6"><i class="fa-solid fa-star"></i></a>
                            </div>
                            <div class="card-body">
                                <span class="f-size-4">Acesso Imediato</span>
                                <p class="text-color-white mt-2 f-size-1 font-weight-light">
                                    Seu login e senha para acessar o produto serão enviados
                                    ao seu e-mail logo após o processamento do pagamento.
                                </p>
                            </div>
                        </div>
                    </div>
                <div class="col-md-2"></div>
            </div>
        </div>
    </div>
</div>