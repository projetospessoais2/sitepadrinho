<div class="background-default banner1 h-100 container-fluid">
    <div class="mt-neg-10 show-element"></div>
    <div class="row">
        <!-- <div class="col-md-1"></div> -->
        <div class="col-md-6 mt-4 text-center">
            <img src="assets/sussurro/logo-sussurro.webp" class="card-img" style="width: 100%; height: 100%">
        </div>
        <div class="col-md-5 mt-5 text-center">
            <div class="mt-neg-18 show-element"></div>
            <div class="text-color-white neon-behavior-white-light f-size-1-7 text-weight-bold transform-upper lh-1 title">
                Já imaginou poder causar o <span class="vermelho-principium neon-behavior-red-intense">ápice do prazer</span> em qualquer pessoa usando apenas a <span class="vermelho-principium neon-behavior-red-intense">sua voz</span>?
            </div>

            <div class="ratio ratio-16x9 mt-4">
                <lite-youtube videoid="Qq0klhMhyN4" style="background-image: url('https://i.ytimg.com/vi/Qq0klhMhyN4/hqdefault.jpg');">
                    <a href="https://youtube.com/watch?v=Qq0klhMhyN4" class="lty-playbtn" title="Play Video">
                        <span class="lyt-visually-hidden">Play Video: Keynote (Google I/O '18)</span>
                    </a>
                </lite-youtube>
            </div>

            <a class="btn btn-lg col-12 mt-5 btn-principium text-color-white f-size-1-2 neon-behavior-white-medium" href="#navigate">
                QUERO GARANTIR A MINHA VAGA
            </a>

        </div>
        <div class="col-md-1"></div>
    </div>
    <br><br>
</div>

<div class="background-default banner2 container-fluid">
    <div class="row reverse">
        <div class="col-md-1"></div>
        <div class="col-md-5 mt-8" id="divClassButtonInitial">
            <p class="f-size-1-9 vermelho-principium transform-upper text-weight-bolder">
                Pra quem é O sussurro tântrico?
            </p>
            <p class="text-color-white letter-space-0-8 f-size-1-1 text-weight-light neon-behavior-white-intense">
                Se você sente<span class="neon-behavior-white-intense text-weight-bolder"> insegurança</span> em qualquer área da vida, está cansado de se sentir insuficiente e quer se <span class="neon-behavior-white-intense text-weight-bolder">libertar</span>
                de travas que te impedem de sentir e proporcionar <span class="neon-behavior-white-intense text-weight-bolder">PRAZER DE VERDADE</span>, coisas que te impedem de causar e sentir <span class="neon-behavior-white-intense text-weight-bolder">PRAZER de verdade</span>, se tornando cada vez mais <span class="neon-behavior-white-intense text-weight-bolder">PODEROSO</span> usando uma coisa que você já tem:<span class="neon-behavior-white-intense text-weight-bolder"> A SUA PRÓPRIA VOZ,</span> o Sussurro Tântrico é pra você.
            </p>
        </div>
        <div class="mt-12 show-element"></div>
        <div class="col-md-6">
            <img src="assets/sussurro/img-ss.webp" class="card-img-top overflow" id="img-overflow" alt="...">
        </div>
        <div class="row mt-7"></div>
    </div>
</div>


<div class="background-default banner3 h-100 container-fluid">
    <div class="row">
        <div class="col-md-1"></div>
        <div class="col-md-10 text-color-white mt-10 text-center">
            <div class="mt-neg-18 show-element"></div>
            <p class="neon-behavior-white-intense f-size-2-5 text-weight-bolder">
                O QUE VOCÊ VAI APRENDER NO CURSO:
            </p>

            <div class="row text-color-white mt-5">
                <div class="col-md-4 mt-5">

                    <div class="card position-relative border-principium bg-transparent">
                        <img src="assets/sussurro/hipnose-prazer.webp" class="img-card size-1 rounded-circle" alt="...">
                        <div class="card-body mt-5 text-center no-shadow">
                            <p class="transform-upper text-weight-bold">
                                Hipnose para prazer
                            </p>
                            <p class="f-size-0-8">
                            Aqui você aprenderá a como usar a hipnose e elevar seu nível para sentir e proporcionar MAIS PRAZER.
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 mt-5">

                    <div class="card position-relative border-principium bg-transparent">
                        <img src="assets/principium/body.webp" class="img-card size-1 rounded-circle" alt="...">
                        <div class="card-body mt-5 text-center no-shadow">
                            <p class="transform-upper text-weight-bold">
                                Hipnose para emagrecer
                            </p>
                            <p class="f-size-0-8">
                            Já imaginou poder usar a hipnose para educar seu corpo e mente para atingir aquele corpo que tanto quer? É exatamente isso que você vai aprender.
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 mt-5">

                    <div class="card position-relative border-principium bg-transparent">
                        <img src="assets/sussurro/procastinacao.webp" class="img-card size-2 rounded-circle" alt="...">
                        <div class="card-body mt-5 text-center no-shadow">
                            <p class="transform-upper text-weight-bold">
                            HIPNOSE PARA PROCRASTINAÇÃO
                            </p>
                            <p class="f-size-0-8">
                            A procrastinação é um dos piores inimigos de um objetivo que você quer alcançar. Mas e se nós pudéssemos usar a hipnose para acabar com ela? É isso que o Padrinho vai te ensinar aqui.
                            </p>
                        </div>
                    </div>
                </div>
            </div>


            <div class="row text-color-white mt-2">
                <div class="col-md-4 mt-5">

                    <div class="card position-relative border-principium bg-transparent">
                        <img src="assets/sussurro/enriquecer.webp" class="img-card size-1 rounded-circle" alt="...">
                        <div class="card-body mt-5 text-center no-shadow">
                            <p class="transform-upper text-weight-bold">
                                HIPNOSE PARA ENRIQUECER
                            </p>
                            <p class="f-size-0-8">
                            Atrair o dinheiro que você quer não é tão difícil assim. Você só precisa saber como fazer isso da forma certa. Nessa parte você aprenderá como fazer isso junto com o Kaula Tantra, usando sua própria energia. Só que com aquele jeitinho diferente que só o Padrinho ensina.
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 mt-5">

                    <div class="card position-relative border-principium bg-transparent">
                        <img src="assets/sussurro/esquecer-problemas.webp" class="img-card size-1 rounded-circle" alt="...">
                        <div class="card-body mt-5 text-center no-shadow">
                            <p class="transform-upper text-weight-bold">
                            HIPNOSE PARA ESQUECER PROBLEMAS
                            </p>
                            <p class="f-size-0-8">
                            O fato de não conseguirmos estar presentes para saber receber todas as oportunidades que a vida nos oferece, geralmente está ligado ao fato de estarmos com a cabeça cheia pensando em todos os nossos problemas. Mas com a hipnose, temos uma permissão para acessar e ficar com a mente “vazia”, totalmente presente em cada momento, conseguindo desfrutar de cada um como se fosse único.
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 mt-5">

                    <div class="card position-relative border-principium bg-transparent">
                        <img src="assets/sussurro/ansiedade.webp" class="img-card size-2 rounded-circle" alt="...">
                        <div class="card-body mt-5 text-center no-shadow">
                            <p class="transform-upper text-weight-bold">
                            HIPNOSE PARA ANSIEDADE
                            </p>
                            <p class="f-size-0-8">
                            A ansiedade, geralmente está ligada ao “excesso de futuro”, em que não conseguimos desfrutar e nos concentrar no que está acontecendo agora, pois estamos preocupados com algo que pode ou vai acontecer. Mas sabendo usar a hipnose da forma certa, conseguimos inibir essa preocupação, nos mantendo mais presentes e aprendendo que CONFIAR é muito mais gostoso do que se PREOCUPAR.
                            </p>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <div class="col-md-1"></div>
    </div>

    <br><br>
</div>

<div class="background-default banner4 h-100 container-fluid">
    <div class="row">
        <div class="col-md-1"></div>
        <div class="col-md-10 text-color-white mt-5 text-center">
            <p class="f-size-2 text-weight-bolder">
                O QUE OS ALUNOS FALAM SOBRE O SUSSURRO TÂNTRICO...
            </p>

            <div class="row">
                <div class="col-md-3"></div>
                <div class="col-md-6 text-center">            
                    <div class="ratio ratio-16x9 mt-4">
                        <lite-youtube videoid="QR4fgUYzs_c" style="background-image: url('https://i.ytimg.com/vi/QR4fgUYzs_c/hqdefault.jpg');">
                            <a href="https://youtube.com/watch?v=QR4fgUYzs_c" class="lty-playbtn" title="Play Video">
                                <span class="lyt-visually-hidden">Play Video: Keynote (Google I/O '18)</span>
                            </a>
                        </lite-youtube>
                    </div>
                </div>
                <div class="col-md-3"></div>
            </div>

            <div class="row mt-5"> 
                <div class="col-md-6">
                    <img class="card-img" src="assets/sussurro/feedbacks/feedback-1.webp" alt="Card image cap" style="width: 100%; height: 34%">
                    <img class="card-img" src="assets/sussurro/feedbacks/feedback-2.webp" alt="Card image cap" style="width: 100%; height: 30%">
                    <img class="card-img mt-3" src="assets/sussurro/feedbacks/feedback-3.webp" alt="Card image cap" style="width: 100%; height: 34%">
                </div>
                <div class="col-md-6">
                    <img class="card-img" src="assets/sussurro/feedbacks/feedback-4.webp" alt="Card image cap" style="width: 100%; height: 40%">
                    <img class="card-img" src="assets/sussurro/feedbacks/feedback-5.webp" alt="Card image cap" style="width: 100%; height: 16%">
                    <img class="card-img" src="assets/sussurro/feedbacks/feedback-6.webp" alt="Card image cap" style="width: 100%; height: 36%">
                </div>
            </div>
        </div>
        <div class="col-md-1"></div>
    </div>
    <br><br><br>
</div>