<header class="masthead">
    <div class="container h-75">

        <div class="row h-80 align-items-end">
            <div class="col-12 text-center mt-3">

                <img class="rounded-circle" src="assets/perfil.webp" data-holder-rendered="true" >

                <h1 class="neon-red">O PADRINHO</h1>
                <p class="neon-white">@dicasdopadrinho</p>
            </div>
        </div>

        <div class="row h-20 align-items-end">
            <div class="col-12 text-center">
                <h1 class="neon-white saiba-mais pt-5">Links Úteis</h1>
                <a class="neon-white icon-home" href="#topbar"><i class="fa-solid fa-angles-down"></i></a>
            </div>
        </div>

    </div>
</header>

<body>
    <div class="container-fluid" id="topbar">

        <div class="row">
            <div class="col-md-1"></div>
            <div class="col-md-5 text-center mt-3">
                <div class="card">
                    <a href="./principium">
                        <img class="card-img-top" src="assets/home/banner_principium.webp" style="width: 100%; height: 100%">
                    </a>
                </div>
            </div>
            <div class="col-md-5 text-center mt-3">
                <div class="card">
                    <a href="./sussurro">
                        <img class="card-img-top" src="assets/home/banner_susurro_tantrico.webp" style="width: 100%; height: 100%">
                    </a>
                </div>
            </div>
            <div class="col-md-1"></div>
        </div>

        <div class="row">
            <div class="col-md-1"></div>
            <div class="col-md-5 text-center mt-3">
                <div class="card">
                    <a href="./preliminar">
                        <img class="card-img-top" src="assets/home/banner_preliminar.webp" style="width: 100%; height: 100%">
                    </a>
                </div>
            </div>
            <div class="col-md-5 text-center mt-3">
                <div class="card">
                    <a href="./cativar">
                        <img class="card-img-top" src="assets/home/banner_cativar.webp" style="width: 100%; height: 100%">
                    </a>
                </div>
            </div>
            <div class="col-md-1"></div>
        </div>

        <div class="row">
            <div class="col-md-1"></div>
            <div class="col-md-5 text-center mt-3">
                <div class="card">
                    <a href="./libertar">
                        <img class="card-img-top" src="assets/home/banner_libertar.webp" style="width: 100%; height: 100%">
                    </a>
                </div>
            </div>
            <div class="col-md-5 text-center mt-3">
                <div class="card">
                    <a href="./despertar">
                        <img class="card-img-top" src="assets/home/banner_despertar.webp" style="width: 100%; height: 100%">
                    </a>
                </div>
            </div>
            <div class="col-md-1"></div>
        </div>

        <div class="row">
            <div class="col-md-1"></div>
            <div class="col-md-5 text-center mt-3">
                <div class="card">
                    <a href="./encantar">
                        <img class="card-img-top" src="assets/home/banner_encantar.webp" style="width: 100%; height: 100%">
                    </a>
                </div>
            </div>
            <div class="col-md-5 text-center mt-3">
                <div class="card">
                    <a href="https://www.dicasdopadrinho.com/delirium">
                        <img class="card-img-top" src="assets/home/banner_delirium.webp" style="width: 100%; height: 100%">
                    </a>
                </div>
            </div>
            <div class="col-md-1"></div>
        </div>

        <div class="row">
            <div class="col-md-1"></div>
            <div class="col-md-5 text-center mt-3">
                <div class="card">
                    <a href="https://www.youtube.com/c/DicasdoPadrinho">
                        <img class="card-img-top" src="assets/home/banner_youtube.webp" style="width: 100%; height: 100%">
                    </a>
                </div>
            </div>
            <div class="col-md-5 text-center mt-3">
                <div class="card">
                    <a href="https://open.spotify.com/show/1yZiGRYl7nxchzfDjMOd0F?si=XamMMkGKT66tYO2WfJofDQ&nd=1">
                        <img class="card-img-top" src="assets/home/banner_cast.webp" style="width: 100%; height: 100%">
                    </a>
                </div>
            </div>
            <div class="col-md-1"></div>
        </div>

        <div class="row">
            <div class="col-md-1"></div>
            <div class="col-md-5 text-center mt-3">
                <div class="card">
                    <a href="https://t.me/dicasdopadrinho">
                        <img class="card-img-top" src="assets/home/banner_telegram.webp" style="width: 100%; height: 100%">
                    </a>
                </div>
            </div>
            <div class="col-md-5 text-center mt-3">
                <div class="card">
                    <a href="https://excessus.com.br/">
                        <img class="card-img-top" src="assets/home/banner_loja.webp" style="width: 100%; height: 100%">
                    </a>
                </div>
            </div>
            <div class="col-md-1"></div>
        </div>


        <div class="row mt-4">
            <div class="col-md-2"></div>
            <div class="col-md-8">
                <div class="card bg-transparent">
                    <div class="card-body text-center">
                        <p class="neon-white footer-home"> CADASTRE-SE PARA NOVIDADES DO PADRINHO </p>

                        <div class="input-group mb-2">
                            <input type="text" class="form-control" placeholder="E-mail" aria-label="Username" aria-describedby="basic-addon1">
                        </div>

                        <div class="input-group mb-2">
                            <input type="text" class="form-control" placeholder="Telefone" aria-label="Username" aria-describedby="basic-addon1">
                        </div>

                        <button type="button" class="btn btn-danger col-12">Enviar</button>

                    </div>
                </div>
            </div>
            <div class="col-md-2"></div>
        </div>

        <br><br><br>
    </div>

</body>