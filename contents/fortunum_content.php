<div class="background-default banner1 h-100 container-fluid">

    <div class="row">
        <div class="col-md-6 text-center mt-10 divimg">
            <img class="card-img section-header-img" src="assets/fortunum/imgheader.webp" alt="Card image cap">
        </div>
        <div class="col-md-5 text-center">
            <div class="row">
                <div class="col-md-1"></div>

                <div class="col-md-10 mt-4 divimg">

                    <div class="row hide-element">
                        <div class="col-md-1"></div>
                        <div class="col-md-10">
                            <img class="card-img logo" src="assets/fortunum/fortunum_logo.webp">
                        </div>
                        <div class="col-md-1"></div>
                    </div>

                    <p class="text-color-white text-weight-bold f-size-1-9 lh-1 transform-upper">
                        Como fazer parte do 1º grupo de Networking Tântrico do Brasil e ter acesso direto ao Padrinho que vai te guiar para experiências incríveis nos próximos 12 meses.
                    </p>

                    <p class="amarelo-fortunum transform-upper f-size-1-1 text-weight-bold">
                        ASSISTA O VÍDEO ABAIXO.
                    </p>
        
                    <div class="ratio ratio-16x9 mt-3">
                        <lite-youtube videoid="RaniKR4hSsY" style="background-image: url('https://i.ytimg.com/vi/RaniKR4hSsY/hqdefault.jpg');">
                            <a href="https://youtube.com/watch?v=RaniKR4hSsY" class="lty-playbtn" title="Play Video">
                                <span class="lyt-visually-hidden">Play Video: Keynote (Google I/O '18)</span>
                            </a>
                        </lite-youtube>
                        <!-- <iframe class="embed-responsive-item lazyframe" src="" data-src="https://www.youtube.com/embed/RaniKR4hSsY"></iframe> -->
                    </div>

                    <p class="amarelo-fortunum f-size-0-8 mt-4 font-weight-medium">
                        PRÉ-INSCRIÇÃO NETWORKING TÂNTRICO
                    </p>

                    <a href="https://checkout.mycheckout.com.br/checkout/6245c358ff7cc27c1b3fb33c?">
                        <button class="btn btn-lg col-12 mt-2 btn-participar text-black text-weight-bolder f-size-1-1 text-center" id="inicial">
                            EU QUERO PARTICIPAR DO EVENTO DE<br>LANÇAMENTO DO NETWORKING TÂNTRICO
                        </button>
                    </a>
                    <div class="col-12 text-center">
                        <p class="text-color-white lh-1-2 f-size-1-2 mt-3">
                            12 de abril às 19hrs
                        </p>
                    </div>

                </div>

                <div class="col-md-1"></div>
            </div>

        </div>
        <div class="col-md-1"></div>
    </div>

    <br><br>

</div>
