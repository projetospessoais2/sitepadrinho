<div class="background-default banner1 h-100 container-fluid">

    <div class="mt-neg-15 show-element"></div>

    <div class="row">
        <div class="col-md-3"></div>
        <div class="col-md-6 mt-8">

            <img class="card-img" src="assets/encantar/encantar_logo.webp" style="width: 100%; height: 100%">
        </div>
        <div class="col-md-3"></div>
    </div>

    <div class="mt-neg-15 show-element"></div>

    <div class="row">
        <div class="col-md-1 resize-bs-col-md-1-upper"></div>
        <div class="col-md-5 mt-8 resize-bs-col-md-5-lower" id="divClassButtonInitial">

            <div class="text-color-white text-weight-bold f-size-1-6 transform-upper lh-1">
                Use o poder do ENCANTAR para quebrar os padrões de oratória e usar a sua voz para se tornar uma pessoa mais poderosa e confiante.
            </div>

            <p class="azul-encantar-2 mt-3 f-size-1">
                Você vai descobrir que o poder de <span class="text-weight-bold">ENCANTAR</span> as pessoas é muito mais eficaz do que qualquer técnica de conversas ou script de vendas que te ensinaram e, ainda, vencer a timidez.
            </p>

        </div>
        <div class="col-md-5 mt-6 text-center resize-bs-col-md-5-lower">

            <div class="mt-neg-10 show-element"></div>
            <div class="ratio ratio-16x9">
                <lite-youtube videoid="zanZvVlZjZE" style="background-image: url('https://i.ytimg.com/vi/zanZvVlZjZE/hqdefault.jpg');">
                    <a href="https://youtube.com/watch?v=zanZvVlZjZE" class="lty-playbtn" title="Play Video">
                        <span class="lyt-visually-hidden">Play Video: Keynote (Google I/O '18)</span>
                    </a>
                </lite-youtube>
            </div>

        </div>
        <div class="col-md-1 resize-bs-col-md-1-upper"></div>
    </div>

    <div class="row">
        <div class="col-md-1 resize-bs-col-md-1-upper"></div>
        <div class="col-md-5 text-center resize-bs-col-md-5-lower" id="divClassButtonInitial">
            <a class="" href="#navigate">
                <button class="btn btn-lg col-12 mt-4 btn-encantar text-color-white text-weight-bold f-size-1-1">QUERO APRENDER A ENCANTAR AS PESSOAS</button>
            </a>
        </div>
        <div class="col-md-6"></div>
    </div>


    <br><br>

</div>

<div class="background-default banner2 h-100 container-fluid">

    <div class="row">
        <div class="col-md-1"></div>
        <div class="col-md-10 mt-5 text-center">

            <p class="f-size-1 text-color-white lh-1-2">
                Você já parou para pensar que a <span class="text-weight-bold">timidez</span> está fechando as portas para oportunidades magníficas na sua vida?
            </p>

            <p class="f-size-1 text-color-white lh-1-2">
                Quantas vezes <span class="text-weight-bold">você deixou</span> de conhecer novas pessoas porque a sua timidez te atrapalhou a desenrolar uma conversa?
            </p>

            <p class="f-size-1 text-color-white lh-1-2">
                Quantas vezes a <span class="text-weight-bold">ansiedade</span> te atrapalhou em resolver situações, que pareciam simples mas se tornaram complicadas?
            </p>

            <p class="f-size-1 text-color-white lh-1-2">
                Quantos clientes já foram <span class="text-weight-bold">perdidos</span>, porque o vendedor ficou ansioso e seguiu scripts de venda na hora de negociar um produto?
            </p>

            <p class="mt-6 azul-encantar f-size-2 text-weight-bold lh-1-2">
                ISSO ACABA HOJE
            </p>

            <p class="f-size-1 text-color-white mt-6 lh-1-2">
                E o que você está prestes a ver nas próximas linhas vai te ajudar a se transformar em uma pessoa <span class="text-weight-bold">poderosa, confiante e preparada</span> para situações onde você precisa se comunicar com outras pessoas.
            </p>

            <p class="f-size-1 text-color-white lh-1-2">
                Além de tornar qualquer conversa ou <span class="text-weight-bold">negociação mais interessante</span>, sem o mesmo clichê de sempre.
            </p>

            <p class="f-size-1 text-color-white lh-1-2">
                Você precisa fazer um <span class="text-weight-bold">DETOX</span> sobre tudo o que já escutou ou aprendeu sobre como <span class="text-weight-bold">melhorar a oratória</span> e <span class="text-weight-bold">acabar com a timidez</span> na sua vida.
            </p>


            <p class="mt-6 azul-encantar f-size-2-3 text-weight-bolder lh-1-2">
                VOCÊ ESTÁ SENTINDO VONTADE DE APRENDER?
            </p>

            <p class="text-color-white f-size-4 transform-upper lh-1-2 neon-behavior-white-intense text-weight-medium">
                Se liga em tudo que vai encontrar no ENCANTAR:
            </p>


        </div>
        <div class="col-md-1"></div>
    </div>

    <div class="row mt-3">
        <div class="col-md-2"></div>
        <div class="col-md-8">
            <!-- <div class="card-group text-color-white"> -->

            <div class="row text-color-white">
                <div class="col-md-4 mt-5">

                    <div class="card position-relative border-encantar bg-transparent neon-behavior-blue-medium">
                        <img src="assets/encantar/energy.webp" class="img-card size-1 rounded-circle" alt="...">
                        <div class="card-body mt-5 text-center text-weight-bold no-shadow">
                            Como usar o poder da “ENERGIA IDEAL” para quebrar o gelo e vencer a TIMIDEZ;
                        </div>
                    </div>
                </div>
                <div class="col-md-4 mt-5">

                    <div class="card position-relative border-encantar bg-transparent neon-behavior-blue-medium">
                        <img src="assets/encantar/sells.webp" class="img-card size-2 rounded-circle" alt="...">
                        <div class="card-body mt-5 text-center text-weight-bold no-shadow">
                            Como vender “SEM VENDER” na hora de apresentar toda e qualquer idéia;
                        </div>
                    </div>
                </div>
                <div class="col-md-4 mt-5">

                    <div class="card position-relative border-encantar bg-transparent neon-behavior-blue-medium">
                        <img src="assets/encantar/W.webp" class="img-card size-1 rounded-circle" alt="...">
                        <div class="card-body mt-5 text-center text-weight-bold no-shadow">
                            “O PODER DO W” usada por palestrantes e filmes Hollywoodianos para prender a atenção;
                        </div>
                    </div>
                </div>
            </div>


            <div class="row text-color-white mt-2">
                <div class="col-md-4 mt-5">

                    <div class="card position-relative border-encantar bg-transparent neon-behavior-blue-medium">
                        <img src="assets/encantar/1.webp" class="img-card size-1 rounded-circle" alt="...">
                        <div class="card-body mt-5 text-center text-weight-bold no-shadow">
                            A técnica do CCG que me fez bater metas e me tornar o melhor vendedor da minha antiga empresa;
                        </div>
                    </div>
                </div>
                <div class="col-md-4 mt-5">

                    <div class="card position-relative border-encantar bg-transparent neon-behavior-blue-medium">
                        <img src="assets/encantar/negociacao.webp" class="img-card size-2 rounded-circle" alt="...">
                        <div class="card-body mt-5 text-center text-weight-bold no-shadow">
                            Aprenda negociar sem depender de Scripts ultrapassados;
                        </div>
                    </div>
                </div>
                <div class="col-md-4 mt-5">

                    <div class="card position-relative border-encantar bg-transparent neon-behavior-blue-medium">
                        <img src="assets/encantar/hand.webp" class="img-card size-2 rounded-circle" alt="...">
                        <div class="card-body mt-5 text-center text-weight-bold no-shadow">
                            Como realmente ENCANTAR em todas as conversas ou apresentação com público;
                        </div>
                    </div>
                </div>
            </div>


            <!-- </div> -->
        </div>
        <div class="col-md-2"></div>
    </div>

    <br><br>

</div>

<div class="background-default banner3 h-100 container-fluid">

    <div class="row">
        <div class="col-md-1"></div>
        <div class="col-md-5 mt-8 text-center">
            <div class="mt-neg-15 show-element"></div>
            <div class="text-color-white text-weight-bold f-size-1-7">
                O QUE FALAM SOBRE O ENCANTAR...
                <div class="row mt-4">
                    <div class="col-md-6">
                        <img class="card-img" src="assets/encantar/feedbacks/feedback01.webp" style="width: 100%; height: 28%">
                        <img class="card-img" src="assets/encantar/feedbacks/feedback04.webp" style="width: 100%; height: 25%">
                        <img class="card-img mt-3" src="assets/encantar/feedbacks/feedback06.webp" style="width: 100%; height: 22%">
                        <img class="card-img mt-3" src="assets/encantar/feedbacks/feedback08.webp" style="width: 100%; height: 24%">
                    </div>
                    <div class="col-md-6">
                        <img class="card-img" src="assets/encantar/feedbacks/feedback02.webp" style="width: 100%; height: 19%">
                        <img class="card-img" src="assets/encantar/feedbacks/feedback03.webp" style="width: 100%; height: 19%">
                        <img class="card-img" src="assets/encantar/feedbacks/feedback05.webp" style="width: 100%; height: 34%">
                        <img class="card-img" src="assets/encantar/feedbacks/feedback07.webp" style="width: 100%; height: 28%">
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-5 mt-10 text-center">
            <div class="mt-neg-30 show-element"></div>
            <img class="card-img" src="assets/encantar/capa_encantar.webp">
        </div>
        <div class="col-md-1"></div>
    </div>

    <br><br>

</div>

<div class="background-default banner4 h-100 container-fluid">
    <div class="row text-center text-color-white letter-space-1-5 lh-1-2 f-size-1-2">
        <div class="row mt-10 text-center">

            <div class="col-md-3"></div>
            <div class="col-md-6 text-color-white lh-1-2">

                <div class="mt-neg-45 show-element"></div>
                <div class="text-weight-extrabold lh-1 f-size-2-2 transform-upper">
                    O que te espera
                    dentro do <span class="azul-encantar-2">ENCANTAR?</span>
                </div>
            </div>
            <div class="col-md-3"></div>
        </div>

        <div class="row mt-4 text-center">
            <div class="col-md-1"></div>
            <div class="col-md-10">
                <div class="mt-neg-10 show-element"></div>
                <div class="card bg-transparent">
                    <div class="mt-neg-20 show-element"></div>
                    <img class="card-img-top sumario-encantar" src="assets/encantar/sumario.webp" style="width: 100%; height: 100%">
                </div>
            </div>
            <div class="col-md-1"></div>
        </div>
    </div>
    <br><br>
</div>

<div class="background-default banner5 h-100 container-fluid">
    <div class="row">
        <div class="col-md-1"></div>
        <div class="col-md-5 text-center">
            <div class="mt-neg-10 show-element"></div>
            <img class="card-img-top overflow" src="assets/encantar/padrinho.webp" alt="Card image cap">
        </div>
        <div class="col-md-5 mt-6 text-color-white text-weight-bolder text-over-mobile" id="divClassButtonInitial">
            <p class="f-size-3 transform-upper text-hidden-mobile">
                ACHOU QUE TINHA ACABADO?
            </p>
            <div class="mt-neg-35 show-element"></div>
            <p class="f-size-4">
                – Além do livro digital, o Padrinho gravou um video falando sobre algumas técnicas ensinadas no livro, contextualizando os capítulos, além de uma aula exclusiva sobre o CHOCOLATINHO!
            </p>
        </div>
        <div class="col-md-1">
        </div>
    </div>
    <br><br>
</div>

<div class="background-default banner4 h-100 container-fluid">
    <div class="row text-center text-color-white letter-space-1-5 lh-1-2 f-size-1-2">
        <div class="row mt-10 text-center">

            <div class="col-md-4"></div>
            <div class="col-md-4 text-color-white lh-1-2">
                <div class="mt-neg-15 show-element"></div>
                <img class="card-img-top" src="assets/encantar/encantar_logo.webp">
            </div>
            <div class="col-md-4"></div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-1"></div>
        <div class="col-md-10 text-color-white f-size-1-2 text-center text-weight-bold hide-element">
            <p class="mt-5">
                Agora que você entendeu como o poder do ENCANTAR agiu na vida dessas pessoas.
            </p>
            <p class="mt-1">
                E também, como ele pode te ajudar a ter segurança para aproveitar as oportunidades que aparecem na sua vida.
            </p>
            <p class="mt-1">
                Sem timidez!
            </p>
            <p class="mt-1">
                Sem insegurança!
            </p>
            <p class="mt-1">
                Sem ansiedade!
            </p>
        </div>

        <div class="col-md-10 text-color-white f-size-1-2 text-center text-weight-bold show-element mt-4">
            Dá uma olhada em tudo que você terá acesso levando o <span style="color: #0055ff;"> ENCANTAR</span> ainda hoje:
        </div>

        <div class="col-md-1"></div>
    </div>

    <div class="row mt-5">

        <div class="col-md-3"></div>
        <div class="col-md-6">

            <div class="card mostra-precos" id="navigate">

                <div class="f-size-2-3 text-weight-extrabold text-center mt-3 hide-element">
                    Valor Total: <span class="azul-encantar-3"><s>R$49,69</s></span>
                </div>

                <div class="f-size-3 text-weight-extrabold mt-3 text-center show-element">
                    LIVRO ENCANTAR
                </div>

                <img class="card-img" src="assets/encantar/livro_encantar.webp" style="width: 100%; height: 100%">

                <div class="f-size-3 text-weight-extrabold text-center hide-element">
                    LIVRO ENCANTAR
                </div>

                <div class="card-body text-center">
                    <!-- ROW LIVRO CATIVARS -->
                    <div class="row reverse">
                        <div class="col-md-4">
                            <img class="card-img mt-1" src="assets/encantar/chocolatinho.webp" style="width: 100%; height: 100%">
                        </div>
                        <div class="col-md-8 mt-4 text-justify f-size-1-3 text-weight-extrabold">
                            BÔNUS #1: A tão esperada aula do CHOCOLATINHO
                        </div>
                    </div>

                    <!-- ROW BONUS 1 -->
                    <div class="row mt-3 reverse">
                        <div class="col-md-4">
                            <img class="card-img mt-1" src="assets/encantar/surpresa.webp" style="width: 100%; height: 100%">
                        </div>
                        <div class="col-md-8 mt-4 text-justify f-size-1-3 text-weight-extrabold">
                            BÔNUS #2: UMA SURPRESINHA ESPECIAL DO PADRINHO PRA VOCÊ
                        </div>
                    </div>

                    <!-- ROW BONUS 2 -->
                    <div class="row mt-3 reverse">
                        <div class="col-md-4">
                            <img class="card-img" src="assets/encantar/livros.webp" style="width: 100%; height: 100%">
                        </div>
                        <div class="col-md-8 mt-4 text-justify f-size-1-3 text-weight-extrabold">
                            BÔNUS #3: 10% DE DESCONTO NA COMPRA DE TODOS OS LIVROS
                        </div>
                    </div>

                    <!-- ROW TEXTOS APOS IMAGENS -->
                    <div class="row mt-4">
                        <div class="col-md-12">
                            <div class="f-size-2-2 text-weight-extrabold">
                                Valor Total: <span class="azul-encantar-3"><s>R$49,69</s></span>
                            </div>
                            <div class="text-black text-weight-lighter letter-space-1-5 f-size-1-2">
                                Mas você não vai pagar esse valor hoje… <br>
                                Porque tudo isso vai ser seu por apenas
                            </div>
                            <div class="text-weight-extrabold azul-encantar-3 f-size-6 mt-2">
                                7x de R$ 6,35
                            </div>
                            <div class="text-black text-weight-light f-size-4">
                                ou <b>R$39,69</b> à vista
                            </div>
                            <a href = "https://checkout.mycheckout.com.br/checkout/61f5c7ec411d770253cf4804?">
                                <button class="btn btn-lg col-8 mt-4 btn-encantar maior text-weight-bold text-color-white f-size-1-5">
                                    QUERO COMPRAR AGORA
                                </button>
                            </a>
                            <div class="text-black mt-1">
                                🔒 Seus dados estão seguros
                            </div>

                        </div>

                    </div>

                </div>
            </div>

        </div>
        <div class="col-md-3"></div>
    </div>

    <div class="row text-center mt-3">

        <div class="col-md-2"></div>
        <div class="col-md-4">
            <div class="card text-center bg-transparent">
                <div class="card-header">
                    <a class="azul-encantar-3 f-size-6"><i class="fa-solid fa-star"></i></a>
                </div>
                <div class="card-body">
                    <span class="azul-encantar-3 text-weight-extrabold f-size-4">Pagamento Seguro</span>
                    <p class="text-color-white mt-2 f-size-1 font-weight-light">
                        Ambiente seguro. Seus dados estão
                        protegidos e sua compra é 100% segura.
                    </p>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="card text-center bg-transparent">
                <div class="card-header">
                    <a class="azul-encantar-3 f-size-6"><i class="fa-solid fa-star"></i></a>
                </div>
                <div class="card-body">
                    <span class="azul-encantar-3 text-weight-extrabold f-size-4">Acesso Imediato</span>
                    <p class="text-color-white mt-2 f-size-1 font-weight-light">
                        Seu login e senha para acessar o produto serão enviados
                        ao seu e-mail logo após o processamento do pagamento.
                    </p>
                </div>
            </div>
        </div>
        <div class="col-md-2"></div>
    </div>

    <!-- <br><br><br><br><br><br><br>
    <br><br><br><br><br><br><br> -->
</div>