<div class="background-default banner1 h-100 container-fluid">
    <div class="row">
        <div class="col-md-6 mt-4">
            <img src="assets/principium/principium.webp" class="card-img" style="width: 100%; height: 100%">
        </div>
        <div class="col-md-5 mt-5 text-center title">
            <div class="text-color-white neon-behavior-white-light f-size-1-7 text-weight-bold transform-upper lh-1">
                Aprenda todos os princípios da <span class="vermelho-principium neon-behavior-red-intense">massagem tântrica</span> e domine um poder milenar que pode <span class="vermelho-principium neon-behavior-red-intense">transformar sua vida</span>
            </div>

            <div class="ratio ratio-16x9 mt-4">
                <lite-youtube videoid="xZY0Vw0a8GQ" style="background-image: url('https://i.ytimg.com/vi/xZY0Vw0a8GQ/hqdefault.jpg');">
                    <a href="https://youtube.com/watch?v=xZY0Vw0a8GQ" class="lty-playbtn" title="Play Video">
                        <span class="lyt-visually-hidden">Play Video: Keynote (Google I/O '18)</span>
                    </a>
                </lite-youtube>
            </div>

            <a class="btn btn-lg col-12 mt-5 btn-principium text-color-white f-size-1-2 neon-behavior-white-medium" href="#navigate">
                QUERO GARANTIR A MINHA VAGA
            </a>

        </div>
        <div class="col-md-1"></div>
    </div>
    <br>
</div>

<div class="background-default banner2 h-100 container-fluid">
    <div class="row">
        <div class="col-md-1"></div>
        <div class="col-md-5 mt-8" id="divClassButtonInitial">
            <div class="mt-neg-15 show-element"></div>
            <p class="neon-behavior-red-intense f-size-1-9 text-weight-bold vermelho-principium transform-upper">
                Pra quem é O Principium?
            </p>
            <p class="text-color-white letter-space-0-8 f-size-1-1 text-weight-light neon-behavior-white-intense">
                Quem <span class=" text-weight-bolder">sente e vive</span> com o poder que o tantra oferece, entende que quando todas as nossas inseguranças, medos e travas (principalmente na cama) estão resolvidas, <span class="neon-behavior-white-intense text-weight-bolder">todas as outras áreas da vida fluem de uma forma inexplicavelmente melhor.</span>
                <br><br>
                Se você é essa pessoa que quer resolver de uma vez por todas tudo aquilo que te prende, como inseguranças por <span class="neon-behavior-white-intense text-weight-bolder">medo de brochar</span>, coisas que te impedem de causar e sentir <span class="neon-behavior-white-intense text-weight-bolder">PRAZER de verdade</span>, está cansado e quer se sentir <span class="neon-behavior-white-intense text-weight-bolder">PODEROSO</span>, o principium foi criada exatamente pra você.
            </p>
        </div>
        <div class="col-md-5 text-center mt-3">
            <img src="assets/principium/padrinho_choc.webp" class="card-img imgChocolate">
        </div>
        <div class="col-md-1"></div>
    </div>
</div>


<div class="background-default banner3 h-100 container-fluid">
    <div class="row">
        <div class="col-md-1"></div>
        <div class="col-md-10 text-color-white mt-5 text-center">
            <p class="neon-behavior-white-intense f-size-2-5 text-weight-bolder">
                O QUE VOCÊ VAI APRENDER NO CURSO:
            </p>

            <div class="row text-color-white mt-5">
                <div class="col-md-4 mt-5">

                    <div class="card position-relative border-principium bg-transparent">
                        <img src="assets/principium/paper.webp" class="img-card size-1 rounded-circle" alt="...">
                        <div class="card-body mt-5 text-center no-shadow">
                            <p class="transform-upper text-weight-bold">
                                Princípios do tantra
                            </p>
                            <p class="f-size-0-8">
                                O tantra pode ser mais considerado como uma “filosofia de vida”, no qual todos os princípios que o Padrinho vai te ensinar, vão transformar te guiar por toda a vida.
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 mt-5">

                    <div class="card position-relative border-principium bg-transparent">
                        <img src="assets/principium/body.webp" class="img-card size-1 rounded-circle" alt="...">
                        <div class="card-body mt-5 text-center no-shadow">
                            <p class="transform-upper text-weight-bold">
                                Massagem tântrica
                            </p>
                            <p class="f-size-0-8">
                                Para aprendermos e dominarmos algo, precisamos entender cada detalhe desde o início. Aqui o Padrinho te ensina a massagem tântrica desde o básico, mesmo que você não saiba nada do assunto.
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 mt-5">

                    <div class="card position-relative border-principium bg-transparent">
                        <img src="assets/principium/chocolatinho.webp" class="img-card size-2 rounded-circle" alt="...">
                        <div class="card-body mt-5 text-center no-shadow">
                            <p class="transform-upper text-weight-bold">
                                CHOCOLATINHO
                            </p>
                            <p class="f-size-0-8">
                                O chocolatinho é um jeito diferente de fazer sexo oral em alguém. Quem aplica diz que só o chocolatinho já fez a parceira (o) chegar lá em menos de 5 minutos…
                            </p>
                        </div>
                    </div>
                </div>
            </div>


            <div class="row text-color-white mt-2">
                <div class="col-md-4 mt-5">

                    <div class="card position-relative border-principium bg-transparent">
                        <img src="assets/principium/prazer.webp" class="img-card size-1 rounded-circle" alt="...">
                        <div class="card-body mt-5 text-center no-shadow">
                            <p class="transform-upper text-weight-bold">
                                ORGASMO EXCESSUS
                            </p>
                            <p class="f-size-0-8">
                                Aprenda a somar estímulos de uma forma única, causando o êxtase de uma forma única na cama.
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 mt-5">

                    <div class="card position-relative border-principium bg-transparent">
                        <img src="assets/principium/dinheiro.webp" class="img-card size-1 rounded-circle" alt="...">
                        <div class="card-body mt-5 text-center no-shadow">
                            <p class="transform-upper text-weight-bold">
                                KAULA TANTRA
                            </p>
                            <p class="f-size-0-8">
                                Aqui você aprenderá a atrair tudo que quiser (Dinheiro, Trabalho, Pessoas, etc) usando a energia mais forte que existe: A sua energia SEXUAL.
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 mt-5">

                    <div class="card position-relative border-principium bg-transparent">
                        <img src="assets/principium/proposta.webp" class="img-card size-2 rounded-circle" alt="...">
                        <div class="card-body mt-5 text-center no-shadow">
                            <p class="transform-upper text-weight-bold">
                                PROPOSTA HTF
                            </p>
                            <p class="f-size-0-8">
                                A proposta H.T.F. também conhecida como “Proposta irrecusável”, é um jeito único de propor qualquer coisa para qualquer pessoa, que dificilmente alguém consegue negar.
                            </p>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <div class="col-md-1"></div>
    </div>

    <br><br>
</div>

<div class="background-default banner4 h-100 container-fluid">
    <div class="row">
        <div class="col-md-1"></div>
        <div class="col-md-10 text-color-white mt-5 text-center">
            <div class="mt-neg-15 show-element"></div>
            <p class="f-size-2 text-weight-bolder">
                O QUE OS ALUNOS FALAM SOBRE A PRINCIPIUM...
            </p>
            <div class="row mt-4">
                <div class="col-md-6">
                    <img class="card-img" src="assets/principium/feedbacks/feedback01.webp" alt="Card image cap" style="width: 100%; height: 35%">
                    <img class="card-img" src="assets/principium/feedbacks/feedback03.webp" alt="Card image cap" style="width: 100%; height: 37%">
                    <img class="card-img mt-3" src="assets/principium/feedbacks/feedback05.webp" alt="Card image cap" style="width: 100%; height: 25%">
                </div>
                <div class="col-md-6">
                    <img class="card-img" src="assets/principium/feedbacks/feedback02.webp" alt="Card image cap" style="width: 100%; height: 35%">
                    <img class="card-img" src="assets/principium/feedbacks/feedback04.webp" alt="Card image cap" style="width: 100%; height: 33%">
                    <img class="card-img" src="assets/principium/feedbacks/feedback06.webp" alt="Card image cap" style="width: 100%; height: 25%">
                </div>
            </div>
        </div>
        <div class="col-md-1"></div>
    </div>
    <br><br><br>
</div>