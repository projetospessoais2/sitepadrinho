<div class="background-default banner1 h-100 container-fluid">

    <div class="row">
        <div class="col-md-5"></div>
        <div class="col-md-6 text-center">
            <div class="row">
                <div class="col-md-1"></div>

                <div class="col-md-10 mt-8">

                    <div class="row">
                        <div class="col-md-2"></div>
                        <div class="col-md-8">
                            <img class="card-img" src="assets/libertar/libertar_logo.webp" style="width: 100%; height: 100%">
                        </div>
                        <div class="col-md-2"></div>
                    </div>

                    <p class="text-color-white text-weight-extrabold f-size-2 neon-behavior-white-intense mt-4 lh-1">
                        Qual o seu maior medo nas horas íntimas?
                    </p>

                    <div class="ratio ratio-16x9 mt-3">
                        <lite-youtube videoid="KTCAbZ6YKdc" style="background-image: url('https://i.ytimg.com/vi/KTCAbZ6YKdc/hqdefault.jpg');">
                            <a href="https://youtube.com/watch?v=KTCAbZ6YKdc" class="lty-playbtn" title="Play Video">
                                <span class="lyt-visually-hidden">Play Video: Keynote (Google I/O '18)</span>
                            </a>
                        </lite-youtube>
                    </div>


                    <a  href="#navigate">
                        <button class="btn btn-lg col-10 mt-4 btn-aprender text-color-white text-weight-bolder f-size-1-5" id="inicial">
                            QUERO APRENDER AGORA
                        </button>
                    </a>
                    <div class="col-12 text-center">
                        <p class="text-color-white letter-space-1-5 lh-1-2 f-size-0-8 mt-1">Por apenas 7x de R$ 6,35</p>
                    </div>

                </div>

                <div class="col-md-1"></div>
            </div>

        </div>
        <div class="col-md-1"></div>
    </div>

    <br><br>

</div>


<div class="background-default banner2 h-100 container-fluid">

    <div class="row mobile-order">
        <div class="col-md-1"></div>
        <div class="col-md-5 mt-6" id="divClassButtonInitial">

            <div class="text-weight-extrabold text-color-white f-size-2-5">
                Dentro do <span class="libertar-background text-weight-bold">LIBERTAR</span> você irá descobrir:
            </div>

            <div class="row show-element">
                <div class="mt-5 ml-5">
                    <img class="card-img" src="assets/libertar/livro_libertar.webp" style="width: 100%; height: 100%">
                </div>
            </div>

            <div class="f-size-1-3 letter-space-1-5">
                <p class="text-color-white mt-2">
                    Como é, de fato , <span class="libertar2 text-weight-bolder">DOMINAR</span> seus medos e travas.
                </p>

                <p class="text-color-white mt-2">
                    Mapeando seus <span class="libertar2">traumas e fantasias</span> , por todas as noites, com mais <span class="libertar2">consciência corporal e mental</span> , tratando o medo de brochar ou a famosa <span class="libertar2">ejaculação precoce</span>.
                </p>

                <p class="text-color-white mt-2">
                    Desde a <span class="libertar2">DIFICULDADE PARA GOZAR</span>, até dicas práticas sobre como conhecer seu corpo e o do outro podendo assim sentir <span class="libertar2">MUITO MAIS PRAZER</span> E CONFIANÇA!
                </p>
            </div>

        </div>
        <div class="col-md-1"></div>
        <div class="col-md-4 mt-3 mr-3 mt-5 hide-element">
            <img class="card-img" src="assets/libertar/livro_libertar.webp" style="width: 100%; height: 100%">
        </div>
        <div class="col-md-1"></div>
    </div>

</div>

<div class="background-default banner3 h-100 container-fluid">

    <div class="row">
        <div class="col-md-2"></div>
        <div class="col-md-8 mt-5 text-center">

            <div class="text-weight-extrabold text-color-white f-size-2-5">
                Quer ver o que te espera dentro do <span class="libertar2">LIBERTAR</span>?
            </div>

            <img class="card-img-top mt-4" src="assets/libertar/capitulos.webp" alt="Card image cap">
        </div>
        <div class="col-md-2"></div>
    </div>

</div>

<div class="background-default banner4 h-100 container-fluid" id="navigate">

    <div class="row">
        <div class="col-md-1"></div>
        <div class="col-md-4"></div>
        <div class="col-md-2"></div>
        <div class="col-md-4 text-center f-size-2-2 laranja-libertar text-weight-light neon-behavior-orange-medium hide-element">
            Melhor Oferta
        </div>
        <div class="col-md-1"></div>
    </div>

    <div class="row">
        <div class="col-md-1"></div>
        <div class="col-md-4 mt-2">
            <div class="card">
                <div class="card-body text-center">
                    <div class="text-black">
                        <div class="text-weight-extrabold mt-3 f-size-2-7">
                            LIVRO DIGITAL
                        </div>
                        <div class="mt-1 f-size-2-2">
                            <s>R$49,90</s>
                        </div>
                    </div>
                    <!-- IMAGEM LIVRO LIBERTAR -->
                    <div class="row">
                        <div class="col-md-3"></div>
                        <div class="col-md-6 mt-5">
                            <img class="card-img book-libertar" src="assets/libertar/img-libertar-book.webp" style="width: 100%; height: 100%">
                        </div>
                        <div class="col-md-3"></div>
                    </div>
                    <!-- TEXTO VALORES -->
                    <div class="text-black">
                        <div class="f-size-1-5 mt-2">
                            7x de
                        </div>
                        <div class="f-size-6 text-weight-extrabold mt-2">
                            R$ 6,35
                        </div>
                        <div class="f-size-1-6 mt-1">
                            ou <span class="text-weight-bolder">R$39,69</span> à vista
                        </div>
                    </div>
                    <!-- CALL FOR ACTION -->
                    <div class="row">
                        <div class="col-md-1"></div>
                        <div class="col-md-10">
                            <a href="https://checkout.mycheckout.com.br/checkout/60e4a90fdde6116ba880862a?">
                                <button class="btn btn-lg col-12 mt-4 btn-aprender maior blink text-color-white letter-space-1-5 lh-1-2 f-size-1-5 text-weight-bold" id="meio">QUERO APRENDER AGORA</button>
                            </a>
                        </div>
                        <div class="col-md-1"></div>
                    </div>
                    <!-- IMAGEM PAGAMENTOS -->
                    <img class="card-img mt-3" src="assets/libertar/pagamentos.webp" style="width: 100%; height: 100%">
                    <!-- TEXTO SEGURANCA -->
                    <div class="row">
                        <div class="col-md-2"></div>
                        <div class="col-md-8">
                            <div class="text-black mt-3">
                                🔒 Ambiente seguro. Seus dados estão protegidos e sua compra é 100% segura.
                            </div>
                        </div>
                        <div class="col-md-2"></div>
                    </div>

                </div>
            </div>
        </div>
        <div class="col-md-2"></div>

        <div class="col-md-4 text-center f-size-2-2 laranja-libertar text-weight-light neon-behavior-orange-medium show-element mt-3">
            Melhor Oferta
        </div>

        <div class="col-md-4 mt-2">
            <div class="card oferta text-color-white">
                <div class="card-body oferta text-center">
                    <div class="text-weight-extrabold f-size-2-7 mt-3">
                        LIVRO DIGITAL
                        + ÁUDIO LIVRO
                    </div>
                    <div class="text-weight-bolder f-size-2-2 mt-1 letter-space-1-5 lh-1">
                        Com a voz do PADRINHO!
                    </div>
                    <div class="mt-1 f-size-2-2">
                        <s>R$99,80</s>
                    </div>
                    <!-- IMAGEM OFERTA LIBERTAR -->
                    <img class="card-img mt-2" src="assets/libertar/melhor_oferta.webp" style="width: 100%; height: 100%">
                    <!-- TEXTO VALORES -->
                    <div class="text-color-white">
                        <div class="f-size-1-5 mt-2">
                            12x de
                        </div>
                        <div class="f-size-6 text-weight-extrabold mt-2">
                            R$ 6,97
                        </div>
                        <div class="f-size-1-6 mt-1">
                            ou <span class="text-weight-bolder">R$69,69</span> à vista
                        </div>
                    </div>
                    <!-- CALL FOR ACTION -->
                    <div class="row">
                        <div class="col-md-1"></div>
                        <div class="col-md-10">
                            <a href="https://checkout.mycheckout.com.br/checkout/617ed3b6887f964cb1bb928e?">
                                <button class="btn btn-lg col-12 mt-4 btn-aprender-oferta blink text-black letter-space-1-5 lh-1-2 f-size-1-5 text-weight-bold">QUERO APRENDER AGORA</button>
                            </a>
                        </div>
                        <div class="col-md-1"></div>
                    </div>
                    <!-- IMAGEM PAGAMENTOS -->
                    <img class="card-img mt-3" src="assets/libertar/pagamentos.webp" style="width: 100%; height: 100%">
                    <!-- TEXTO SEGURANCA -->
                    <div class="row">
                        <div class="col-md-2"></div>
                        <div class="col-md-8">
                            <div class="text-black mt-3">
                                🔒 Ambiente seguro. Seus dados estão protegidos e sua compra é 100% segura.
                            </div>
                        </div>
                        <div class="col-md-2"></div>
                    </div>
                </div>
            </div>

        </div>
        <div class="col-md-1"></div>
    </div>

</div>