<div class="background-default h-100 container-fluid">

    <div class="row">
        
        <div class="col-md-1 show-bigsizes-screens"></div>
        <div class="col-md-6 resize-bs-col-md-6-lower">
            <img class="card-img initial" src="assets/obvioSecreto/logo.webp" style="width: 100%; height: 100%">
        </div>
        <div class="col-md-5 text-center mt-10 resize-bs-col-md-5-lower">
            <div class="mt-neg-25 show-element"></div>
            <div class="f-size-1 transform-upper text-white hide-element">
                <span class="textObvioSecreto f-size-1-9 monument-font">
                    <i class="far fa-calendar-alt"></i>
                </span>
                <span class="neon-behavior-white-light monument-font fw-500 text-weight-light letter-space-1-1">08 a 14 de Junho</span>
            </div>
            <div class="<?= $classHeadl ?> transform-upper textObvioSecreto monument-font letter-space-0-8 neon-orange-text mt-2 lh-1">
                <?= $headLine ?>
            </div>
            <div class="f-size-1-1 mt-2 text-white text-weight-bold lh-1-2 neon-white-text">
                <?= $subHeadLine ?>
            </div>

            <div class="f-size-0-8 text-white mt-4">
            A partir do dia 8 de Junho você vai ter acesso a um documentário inédito de 4 episódios para se livrar dessa insegurança e viver experiências incríveis.
            </div>

            <form method="POST" action="https://dicasdopadrinho.activehosted.com/proc.php" id="_form_1_" class="_form _form_1 _inline-form  _dark" novalidate>
                <input type="hidden" name="u" value="1" />
                <input type="hidden" name="f" value="1" />
                <input type="hidden" name="s" />
                <input type="hidden" name="c" value="0" />
                <input type="hidden" name="m" value="0" />
                <input type="hidden" name="act" value="sub" />
                <input type="hidden" name="v" value="2" />
                <input type="hidden" name="or" value="7562834c8d36550bfb194078cf1035d2" />

                <input type="text" class="form-control form-rounded mt-4" id="email" name="email" placeholder="Insira seu melhor E-mail" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$" required>
                <input type="text" class="form-control form-rounded mt-2" id="phone" name="phone" pattern="[0-9()#&+*-=.]+" placeholder="DDD (sem o zero) + Celular">
                <input type="hidden" id="field[3]" name="field[3]" value="" placeholder="" />
                <input type="hidden" id="field[4]" name="field[4]" value="" placeholder="" />
                <input type="hidden" id="field[6]" name="field[6]" value="" placeholder="" />
                <input type="hidden" id="field[8]" name="field[8]" value="<?= $utmTerm ?>" placeholder=""/>
                <input type="hidden" id="field[9]" name="field[9]" value="<?= $todaysDate ?>" placeholder=""/>

                <button class="form-control btn btn-cadastro form-rounded col-12 mt-2 f-size-1-3 text-weight-medium" id="_form_1_submit" type="submit">Cadastrar</button>
            </form>

        </div>
        
        <div class="col-md-1"></div>

    </div>

    <div class="row mt-3">
        <div class="mt-2 show-element"></div>
        <div class="col-md-1 show-bigsizes-screens"></div>
        <div class="col-md-4 textObvioSecreto text-center resize-bs-col-md-4-lower">
            <div class="gobold-font f-size-3-5 neon-orange-text">
                08/06
            </div>
            <div class="monument-font f-size-2-7 neon-orange-text transform-upper">
                A ORIGEM
            </div>
            <div class="monument-font neon-white-text f-size-1">
            Descubra a origem dos seus traumas e inseguranças sobre não satisfazer alguém na cama.
            </div>
        </div>
        <div class="mt-1 show-element"></div>
        <div class="col-md-4 textObvioSecreto text-center">
            <div class="gobold-font f-size-3-5 neon-orange-text">
                10/06
            </div>
            <div class="monument-font f-size-2-7 neon-orange-text transform-upper">
                A LIBERDADE
            </div>
            <div class="monument-font neon-white-text f-size-1">
                Aprenda como se libertar dos traumas e bloqueios que foram impostos a você.
            </div>
        </div>
        <div class="mt-1 show-element"></div>
        <div class="col-md-4 textObvioSecreto text-center resize-bs-col-md-4-lower">
            <div class="gobold-font f-size-3-5 neon-orange-text">
                12/06
            </div>
            <div class="monument-font f-size-2-7 neon-orange-text transform-upper">
                A TRAVESSIA
            </div>
            <div class="monument-font neon-white-text f-size-1">
                Saiba como ser um homem inesquecível e desejado fazendo qualquer mulher agradecer por dormir com você.
            </div>
        </div>
        
        <div class="col-md-1 show-bigsizes-screens"></div>
    </div>

    <div class="row mt-3">
        <div class="col-md-4"></div>
        <div class="col-md-4 textObvioSecreto text-center resize-bs-col-md-4-lower margin-bigsizes">
            <div class="gobold-font f-size-3-5 neon-orange-text">
                14/06
            </div>
            <div class="monument-font f-size-2-7 neon-orange-text transform-upper">
                A ESCOLHA
            </div>
            <div class="monument-font neon-white-text f-size-1">
                Episódio ao vivo.
            </div>
        </div>
        <div class="col-md-4"></div>
    </div>

    <div class="row mt-6">
        <div class="col-md-1 resize-bs-col-md-1-upper"></div>
        <div class="col-md-10 monument-font textObvioSecreto neon-orange-text transform-upper text-center f-size-1-4 lh-1-2 resize-bs-col-md-10-lower-2">
            <div class="mt-neg-7 show-element"></div>
            VOCÊ VAI APRENDER A SE LIVRAR DA INSEGURANÇA E CAUSAR ESTÍMULOS ÚNICOS NA VIDA E NA CAMA COM QUALQUER MULHER.
        </div>
        <div class="col-md-1 resize-bs-col-md-1-upper"></div>
    </div>

    <div class="row mt-3">
        <div class="col-md-2"></div>
        <div class="col-md-8 text-center textObvioSecreto text-white neon-white-text f-size-1">
            Durante esse documentário INÉDITO, o Padrinho vai revelar como se libertar de bloqueios e traumas que te tornam inseguros, e fazer você se tornar uma pessoa f*da e inesquecível para causar o êxtase na cama.
        </div>
        <div class="col-md-2"></div>
    </div>


    <div class="elementor-shape elementor-shape-top mt-4" data-negative="false">
        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1000 100" preserveAspectRatio="none">
            <path class="elementor-shape-fill" d="M500,98.9L0,6.1V0h1000v6.1L500,98.9z"></path>
        </svg>
    </div>

    <div class="row">
        <div class="col-md-1 resize-bs-col-md-1-upper"></div>
        <div class="col-md-5 mt-8 resize-bs-col-md-5-lower">
            <div class="textObvioSecreto monument-font neon-orange-text f-size-3-1" id="divClassButtonInitial">
                PADRINHO
            </div>
            <div class="text-white neon-white-text f-size-1-1" id="divClassButtonInitial">
                Trabalhando no mundo corporativo com bons salários e muitas cobranças, João teve uma crise de pânico pelo excesso de remédios controlados até que encontrou o Tantra e reconheceu seu chamado.
                <br><br>
                Criador do método Excessus, hoje usa as suas redes sociais para ajudar milhares de homens e mulheres com uma abordagem única do Tantra. Mostrando essa arte milenar de uma forma atual, prazer e contemplação, carne e alma. Combinações que em suas técnicas trazem sensações únicas e mudanças reais no modo que as pessoas estão vivendo, lidando com sua sexualidade e do próximo.
            </div>
        </div>
        <div class="col-md-6 mt-2 resize-bs-col-md-6-lower">
            <img class="card-img" src="assets/obvioSecreto/padrinho.webp" style="width: 100%; height: 100%; margin-left: -8%">
        </div>
    </div>

</div>