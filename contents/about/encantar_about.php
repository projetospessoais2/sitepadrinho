<div class="mt-neg-15 show-element"></div>
<!-- TITULO -->
<div class="row reverse">
    <div class="col-md-1 resize-bs-col-md-1-upper"></div>
    <div class="col-md-5 f-size-1-2 text-color-white mt-10 resize-bs-col-md-5-lower" id="divClassButtonInitial">
        <div class="f-size-2 text-weight-extrabold">
            Quem é o <span class="azul-encantar-2">Padrinho?</span>
        </div>

        <img class="card-img show-element" src="assets/encantar/padrinho2.webp" style="width: 100%; height: 100%">

        <div class="mt-2 text-weight-lighter">
            Trabalhando no mundo corporativo com bons salários e muitas cobranças, João teve uma crise de pânico pelo excesso de remédios controlados até que encontrou o Tantra e reconheceu seu chamado. Massoterapeuta Tântrico, hoje usa as suas redes sociais para ajudar milhares de homens e mulheres com uma abordagem única do Tantra. Mostrando essa arte milenar de uma forma atual, prazer e contemplação, carne e alma. Combinações que em suas técnicas trazem sensações únicas e mudanças reais no modo que as pessoas estão vivendo, lidando com sua sexualidade e do próximo.
        </div>

    </div>
    
    <div class="col-md-4 mt-10 text-center resize-bs-col-md-4-lower hide-element">
        <img class="card-img" src="assets/encantar/padrinho2.webp" style="width: 100%; height: 100%">
    </div>

    <div class="col-md-2 resize-bs-col-md-1-upper resize-bs-col-md-1-upper"></div>
</div>

<div class="row mt-5">

    <div class="col-md-1 resize-bs-col-md-1-upper"></div>
    <div class="col-md-10 resize-bs-col-md-10-lower-2">

        <div class="card mostra-precos-v2">

            <div class="card-body text-center">

                <!-- ROW TEXTOS APOS IMAGENS -->
                <div class="row mt-3">
                    <div class="col-md-12">
                        <div class="f-size-2-5 text-weight-extrabold">
                            Valor Total: <span class="azul-encantar-3"><s>R$49,69</s></span>
                        </div>
                        <div class="text-black text-weight-lighter letter-space-1-5 f-size-1-2">
                            Mas você não vai pagar esse valor hoje… <br>
                            Porque tudo isso vai ser seu por apenas
                        </div>
                        <div class="text-weight-extrabold azul-encantar-3 f-size-6 mt-2">
                            7x de R$ 6,35
                        </div>
                        <div class="text-black text-weight-light f-size-4">
                            ou <b>R$39,69</b> à vista
                        </div>
                        <a class="btn btn-lg col-4 mt-3 btn-encantar-pop text-weight-bold text-color-white f-size-1-3"  href="#navigate">
                            QUERO APRENDER AGORA
                        </a>
                        <div class="text-black mt-1">
                            🔒 Seus dados estão seguros
                        </div>

                    </div>

                </div>

            </div>
        </div>

    </div>
    <div class="col-md-1 resize-bs-col-md-1-upper"></div>
</div>