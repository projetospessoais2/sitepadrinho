<!-- TITULO -->
<div class="row">
    <div class="col-md-1 resize-bs-col-md-1-upper"></div>
    <div class="col-md-5 mt-5 text-color-white f-size-1-1 lh-1 work-sans-font text-weight-lighter resize-bs-col-md-5-lower" id="divClassButtonInitial">
        <h2 class="text-color-white f-size-1-9 text-weight-extrabold mb-3">
            Quem é o <span>Padrinho?</span>
        </h2>
        João Vitor Heringer, mais conhecido como Padrinho, <br class="hide-element"> ensina uma nova vertente sobre o Tantra <br class="hide-element">
        e já ajudou milhares de pessoas com seu método. <br class="hide-element">
        Transformando, libertando e ensinando com amor <br class="hide-element"> através das redes sociais, ele traz uma <br class="hide-element">
        perspectiva inédita sobre vários assuntos.
        <br><br>
        Teve seu primeiro contato com o Tantra em seu curso de <br class="hide-element"> Massoterapia, aos 20 anos. E por sentir uma verdade <br class="hide-element"> profunda ligada a sua infância, se interessou cada vez <br class="hide-element"> mais em aprender e ensinar.
        <br><br>
        Tendo se encontrado pelo propósito da libertação e <br class="hide-element"> sentindo o chamado para guiar outras Almas e restaurar <br class="hide-element"> relações através da filosofia do Tantra, <br class="hide-element">
        ele hoje te convida a aprender <br class="hide-element">
        o melhor caminho para se DESPERTAR.
    </div>
    <div class="col-md-5 text-center resize-bs-col-md-5-lower">
        <div class="mt-neg-8 show-element"></div>
        <img class="card-img" src="assets/despertar/padrinho-img.webp" style="width: 100%; height: 100%">
    </div>
    <div class="col-md-1 resize-bs-col-md-1-upper"></div>

    <!-- <div class="col-md-1"></div> -->

    <!-- <div class="background-default antes-footer mt-6 container-fluid">
        <div class="text-center black mt-1">
            <h2 class="f-size-1-9 text-weight-bolder"><span class="antes-footer-text">Aproveite também os outros Livros do Padrinho</span></h2>
        </div>
    </div> -->
</div>