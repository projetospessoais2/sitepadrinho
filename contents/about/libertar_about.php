<div class="mt-neg-10 show-element"></div>
<!-- TITULO -->
<div class="row" id="divClassButtonInitial">
    <div class="col-md-1 resize-bs-col-md-1-upper"></div>
    <div class="col-md-5 mt-5 resize-bs-col-md-5-lower">
        <h2 class="text-color-white f-size-2 about-p text-weight-extrabold neon-behavior-white-light">

            Quem é o <span class="laranja-libertar2 neon-behavior-orange-light">Padrinho?</span>

        </h2>

        <div class="text-color-white letter-space-1-5 mt-4 f-size-1-2 lh-1 work-sans-font text-weight-lighter">
            João Vitor Heringer, mais conhecido como Padrinho, ensina uma nova vertente sobre o Tantra
            e já ajudou milhares de pessoas com seu método.
            Transformando, libertando e ensinando com amor através das redes sociais, ele traz uma
            perspectiva inédita sobre vários assuntos. <br><br>

            Teve seu primeiro contato com o Tantra em seu curso de Massoterapia, aos 20 anos. E por sentir uma verdade profunda ligada a sua infância, se interessou cada vez mais em aprender e ensinar. <br><br>

            Tendo se encontrado pelo propósito da libertação e sentindo o chamado para guiar outras Almas e restaurar relações através da filosofia do Tantra,
            ele hoje te convida a aprender
            o melhor caminho para se LIBERTAR.
        </div>


    </div>
    <div class="col-md-5 text-center resize-bs-col-md-5-lower">
        <img class="card-img" src="assets/libertar/sobre_padrinho.webp" style="width: 100%; height: 100%">
    </div>
</div>

<br><br>