<!-- TITULO -->
<div class="row text-center">
    <div class="col-md-1"></div>
    <div class="col-md-10 mt-8">
        <h2 class="text-color-white f-size-2-5 text-weight-extrabold transform-upper">

            Quem é o <span>Padrinho?</span>

        </h2>
    </div>

    <div class="col-md-1"></div>
</div>

<!-- TITULO -->
<div class="row">
    <div class="col-md-1 resize-bs-col-md-1-upper"></div>
    <div class="col-md-5 text-center resize-bs-col-md-5-lower show-element">
        <img class="card-img" src="assets/preliminar/padrinho-img.webp" style="width: 100%; height: 100%">
    </div>
    <div class="col-md-5 mt-8 text-color-white f-size-1-2 lh-1 work-sans-font text-weight-lighter resize-bs-col-md-5-lower">
    João Vitor Heringer, mais conhecido como Padrinho, ensina uma nova vertente sobre o Tantra e já ajudou milhares de pessoas com seu método.
    Transformando, libertando e ensinando com amor através das redes sociais, ele traz uma perspectiva inédita sobre vários assuntos.
    <br><br>
    Teve seu primeiro contato com o Tantra em seu curso de Massoterapia, aos 20 anos. E por sentir uma verdade profunda ligada a sua infância, se interessou cada vez mais em aprender e ensinar.
    <br><br>
    Tendo se encontrado pelo propósito da libertação e sentindo o chamado para guiar outras Almas e restaurar relações através da filosofia do Tantra, ele hoje te convida a aprender sobre a arte de PRELIMINAR.
    <br><br>
    <span class="text-weight-light">
        "Chega de se iludir com 'manobras isoladas' nas 'preliminares' que antecedem a penetração, o prazer nunca começou entre quatro paredes. PRELIMINAR é um conceito inteiriço, ininterrupto e comprovado de que a vida, com o Tantra, é realmente muito além disso.˜
        (João Vitor Heringer, o Padrinho.)
    </span>
    </div>
    <div class="col-md-5 text-center resize-bs-col-md-5-lower hide-element">
        <img class="card-img" src="assets/preliminar/padrinho-img.webp" style="width: 100%; height: 100%">
    </div>

    <div class="col-md-1 resize-bs-col-md-1-upper"></div>
</div>