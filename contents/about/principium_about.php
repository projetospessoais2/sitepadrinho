<div class="background-lastsection container-fluid">
    <!-- TITULO -->
    <div class="row">
        <div class="col-md-6 text-center">
            <div class="mt-neg-15 show-element"></div>
            <img class="card-img-top overflow" src="assets/principium/padrinho_heart.webp" alt="Card image cap">
        </div>
        <div class="mt-8 show-element">
        </div>
        <div class="col-md-5 mt-5" id="divClassButtonInitial">
            <h2 class="vermelho-principium f-size-2 text-weight-bolder transform-upper">

                Quem é o Padrinho?

            </h2>

            <div class="text-color-white letter-space-1-5 mt-4 f-size-1">
                Trabalhando no mundo corporativo com bons salários e muitas cobranças, João teve uma crise de pânico pelo excesso de remédios controlados até que encontrou o Tantra e reconheceu seu chamado.
                <br><br>
                Massoterapeuta tântrico, hoje usa as suas redes sociais para ajudar milhares de homens e mulheres com uma abordagem única do Tantra. Mostrando essa arte milenar de uma forma atual, prazer e contemplação, carne e alma. Combinações que em suas técnicas trazem sensações únicas e mudanças reais no modo que as pessoas estão vivendo, lidando com sua sexualidade e do próximo.

            </div>
        </div>
        <div class="col-md-1"></div>
        <div class="mt-4 show-element"></div>
    </div>
</div>
