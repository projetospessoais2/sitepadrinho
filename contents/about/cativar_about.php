
<div class="mt-neg-42 show-element"></div>
<!-- TITULO -->
<div class="row text-center">
    <div class="col-md-1"></div>
    <div class="col-md-10 mt-8">
        <h2 class="text-color-white f-size-3 text-weight-extrabold">

            Quem é o <span class="text-weight-extrabold">Padrinho?</span>

        </h2>
    </div>

    <div class="col-md-1"></div>
</div>

<div class="mt-neg-10 show-element"></div>
<!-- TITULO -->
<div class="row">
    <div class="col-md-1 resize-bs-col-md-1-upper"></div>
    <div class="col-md-5 text-center resize-bs-col-md-5-lower show-element">
        <img class="card-img" src="assets/cativar/padrinho_logo.webp" style="width: 100%; height: 100%">
    </div>
    <div class="col-md-5 mt-10 text-color-white f-size-1-2 lh-1 work-sans-font text-weight-lighter resize-bs-col-md-5-lower">

        João Vitor Heringer, mais conhecido como Padrinho, ensina uma nova vertente sobre o Tantra e já ajudou milhares de pessoas com seu método. <br><br>

        Transformando, libertando e ensinando com amor através das redes sociais, ele traz uma perspectiva inédita sobre vários assuntos. <br><br>

        Teve seu primeiro contato com o Tantra em seu curso de Massoterapia, aos 20 anos. E por sentir uma verdade profunda ligada a sua infância, se interessou cada vez mais em aprender e ensinar. <br><br>

        Tendo se encontrado pelo propósito da libertação e sentindo o chamado para guiar outras Almas e restaurar relações através da filosofia do Tantra, ele hoje te convida a aprender sobre a arte de CATIVAR.
        <br>
        <span class="span-lastsection">
            (João Vitor Heringer, o Padrinho.)
        </span>

    </div>
    <div class="col-md-5 text-center resize-bs-col-md-5-lower hide-element">
        <img class="card-img" src="assets/cativar/padrinho_logo.webp" style="width: 100%; height: 100%">
    </div>

    <div class="col-md-1 resize-bs-col-md-1-upper"></div>
</div>