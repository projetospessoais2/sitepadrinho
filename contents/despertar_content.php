<div class="background-default primeira-sessao">
    <div class="mask container-fluid">    
        <!-- TITULO  -->
        <div class="row">
            <div class="col-md-4"></div>
            <div class="col-md-4 text-center">
                <img class="card-img" src="assets/despertar/logo-despertar.webp" style="width: 100%; height: 100%">
            </div>
            <div class="col-md-4"></div>
        </div>
        
       <!-- TEXTO INICIAL E VIDEO (CONTAINER 1) -->
        <div class="row mt-3">
            <div class="col-md-1 resize-bs-col-md-1-upper"></div>
            <div class="col-md-5 text-center resize-bs-col-md-5-lower show-element">
                <div class="ratio ratio-16x9">
                    <lite-youtube videoid="D4oG-bdHOXs" style="background-image: url('https://i.ytimg.com/vi/D4oG-bdHOXs/hqdefault.jpg');">
                        <a href="https://youtube.com/watch?v=D4oG-bdHOXs" class="lty-playbtn" title="Play Video">
                            <span class="lyt-visually-hidden">Play Video: Keynote (Google I/O '18)</span>
                        </a>
                    </lite-youtube>

                </div>
            </div>
            <div class="col-md-5 mt-3 resize-bs-col-md-5-lower" id="divClassButtonInitial">
                <div class="mt-neg-6 show-element"></div>
                <h2 class="text-color-white f-size-2 lh-1 text-weight-bold">
                    COMO SERIA SUA <span>VIDA</span>, SE ACORDASSE MAIS <span>ENÉRGICO</span> TODOS OS DIAS<span>?</span>
                </h2>
                <p class="text-color-white neon-behavior-white-light f-size-1 mt-4 font-weight-medium">
                    Aprenda sobre a sua Energia com o conceito de DESPERTAR que te dará o poder de levar a vida, a rotina e as suas oportunidades de um jeito inédito e poderoso!
                </p>
            </div>
            <div class="col-md-5 text-center mt-3 resize-bs-col-md-5-lower hide-element">
                <div class="ratio ratio-16x9">

                    <lite-youtube videoid="D4oG-bdHOXs" style="background-image: url('https://i.ytimg.com/vi/D4oG-bdHOXs/hqdefault.jpg');">
                        <a href="https://youtube.com/watch?v=D4oG-bdHOXs" class="lty-playbtn" title="Play Video">
                            <span class="lyt-visually-hidden">Play Video: Keynote (Google I/O '18)</span>
                        </a>
                    </lite-youtube>

                </div>
            </div>
            <div class="col-md-1 resize-bs-col-md-1-upper"></div>

        </div>

        <div class="row">
            <div class="col-md-1 resize-bs-col-md-1-upper"></div>
            <div class="col-md-5 mt-2 resize-bs-col-md-5-lower" id="divClassButtonInitial">
                <a class="btn btn-lg col-10 btn-aprender text-color-white letter-space-1-5 lh-1-2 f-size-1-3 text-weight-extrabold" href="#navigate">QUERO APRENDER AGORA</a>
            </div>
            <div class="col-md-6"></div>
        </div>
        <br><br>
    </div>
</div>
    <!-- TEXTO SOBRE O LIVRO (CONTAINER 2) -->
<div class="container-fluid background-default livro">
    <div class="mt-neg-20 show-element"></div>
    <div class="row justify-content-center" id="divClassButtonInitial">
            <div class="col-md-1"></div>
            <div class="col-md-5 mt-8">
                <h2 class="text-color-white f-size-2-5 text-weight-extrabold">Dentro do <span>DESPERTAR</span> você irá descobrir:</h2>
                <p class="text-color-white letter-space-1-5 lh-1-2 f-size-1-2 mt-4">O PODER de sentir <span>TODA A SUA ENERGIA</span> e ser incrivelmente realizado!<br><br>Reconhecendo <span>OPORTUNIDADES MAGNÍFICAS</span>  todos os dias, com mais <span>disposição e foco.</span> O livro traz reflexões e dicas práticas para poder te DESPERTAR de toda preguiça, <span>procrastinação</span> e a ilusão sobre a  <span>falta de tempo.</span><br><br>Desde a <span>PREGUIÇA DE LEVANTAR</span>, até os detalhes que te roubam energia sobre suas metas para você poder ser de fato <span>MUITO MAIS ENÉRGICO</span> DIARIAMENTE!</p>
            </div>
            <div class="col-md-1"></div>
            <div class="col-md-4 card bg-transparent text-center">
                <img src="assets/despertar/livro-despertar.webp" class="card-img book-img-despertar" >
            </div>
            <div class="col-md-1"></div>
        </div>
    </div>
</div>

    <!-- SÚMARIO -->
<div class="background-default black container-fluid">
    <div class="row text-center">
        <h2 class="text-color-white mt-3 f-size-2-5 text-weight-extrabold">O que te espera dentro do <br><span>DESPERTAR</span>?</h2>
    </div>    
    <div class="row text-center mt-4">
        <div class="col-md-3"></div>
            <div class="col-md-6 md-5">
                <div class="card bg-transparent">
                    <img id="sumario" class="card-img-top sumuario-mobile" src="assets/despertar/sumario-despertar.webp" style="width: 100%; height: 100%">
                </div>
            </div>
        <div class="col-md-2"></div>
        <div class="row mt-6"></div>
        <div class="row mt-6"></div>
    </div>
</div>

<div class="background-default yellow container-fluid md-4" id="divClassButtonInitial">
    <div class="row">
        <div class="col-md-2"></div>
        <div class="col-md-3 text-center">
            <img class="card-img-top over" src="assets/despertar/surpresinha-img.webp">
        </div>
        <div class="col-md-5 mt-5 text-color-white text-weight-bolder surpresinha-mobile">
            <p class="transform-upper neon-white shadow-black">
                #SURPRESINHA
            </p>
            <p class="f-size-1-5 text-black">
            E se você gostar da ideia de ter dicas práticas com O Padrinho, ele preparou uma #SURPRESINHA especialmente para você que decidiu DESPERTAR.
            </p>
        </div>
        <div class="col-md-2"></div>
    </div>
    <br><br><br>
</div>

<div class="container-fluid background-default black">   
    <div class="row align-items-center mt-3 justify-content-center text-center">

        <!-- <div class="col-md-1"></div> -->
        <div class="col-md-6">
            <h2 class="f-size-6 letter-space-2 text-weight-bolder"><span>BÔNUS<span></h2>
            <p class="text-white text-weight-bold mt-4 f-size-1-5">Aula bônus com PADRINHO sobre o <br> Chocolatinho</p>
        </div>
    
        <div class="col-md-5 card bg-transparent">
            <img src="assets/despertar/bonus-img.webp" class="card-img" style="width: 100%; height: 100%">
        </div>
        <div class="col-md-1"></div>

    </div>
</div>

<div class="text-center text-color-white background-default black mt-7">
    <h2 class="f-size-2-7 text-weight-extrabold"><span>NOVIDADE</span>!
    <br>Escolha a melhor opção pra você:</h2>
</div>

<div class="background-default black h-100 container-fluid mt-7" id="navigate">
    <div class="row">
        <div class="col-md-1"></div>
        <div class="col-md-4"></div>
        <div class="col-md-2"></div>
        <div class="col-md-1"></div>
    </div>

    <div class="row">
        <div class="col-md-1"></div>
        <div class="col-md-4 mt-2">
            <div class="mt-neg-6 show-element"></div>
            <div class="card">
                <div class="card-body text-center">
                    <div class="text-black">
                        <div class="text-weight-extrabold mt-3 f-size-2-7">
                            LIVRO DIGITAL
                        </div>
                        <div class="mt-1 f-size-2-2">
                            <s>R$49,90</s>
                        </div>
                    </div>
                    <!-- IMAGEM LIVRO DESPERTAR -->
                    <div class="row">
                        <div class="col-md-3"></div>
                        <div class="col-md-6 mt-5">
                            <div class="mt-neg-20 show-element"></div>
                            <img class="card-img" src="assets/despertar/livro-despertar.webp" style="width: 100%; height: 100%">
                        </div>
                        <div class="col-md-3"></div>
                    </div>
                    <div class="mt-neg-20 show-element"></div>
                    <!-- TEXTO VALORES -->
                    <div class="text-black">
                        <div class="f-size-1-5 mt-2">
                            7x de
                        </div>
                        <div class="f-size-6 text-weight-extrabold">
                            R$ 6,35
                        </div>
                        <div class="f-size-1-6">
                            ou <strong>R$39,69</strong> à vista
                        </div>
                    </div>
                    <!-- CALL FOR ACTION -->
                    <div class="row">
                        <div class="col-md-1"></div>
                        <div class="col-md-10">
                            <a href="https://checkout.mycheckout.com.br/checkout/61577b3b6f150b346d8f3b8c?">
                                <button class="btn btn-lg col-12 mt-4 btn-aprender-v2 maior text-color-white letter-space-1-5 lh-1-2 f-size-1-5 text-weight-bold">QUERO APRENDER AGORA</button>
                            </a>
                        </div>
                        <div class="col-md-1"></div>
                    </div>
                    <!-- IMAGEM PAGAMENTOS -->
                    <img class="card-img mt-3" src="assets/libertar/pagamentos.webp" style="width: 100%; height: 100%">
                    <!-- TEXTO SEGURANCA -->
                    <div class="row">
                        <div class="col-md-2"></div>
                        <div class="col-md-8">
                            <div class="text-black mt-3">
                                🔒 Ambiente seguro. Seus dados estão protegidos e sua compra é 100% segura.
                            </div>
                        </div>
                        <div class="col-md-2"></div>
                    </div>

                </div>
            </div>
        </div>
        <div class="col-md-2"></div>

        <div class="col-md-4 text-center f-size-2-2 yellow-despertar text-weight-light neon-behavior-yellow show-element mt-3">
            Melhor Oferta
        </div>

        <div class="col-md-4 mt-2">
            <div class="card oferta text-black background-default yellow">
                <div class="card-body oferta text-center">
                    <div class="text-weight-extrabold f-size-2-7 mt-3">
                        LIVRO DIGITAL
                        + ÁUDIO LIVRO
                    </div>
                    <div class="text-weight-bold f-size-2-2 mt-1 letter-space-1-5 lh-1">
                        Com a voz do PADRINHO!
                    </div>
                    <div class="mt-1 f-size-2-2">
                        <s>R$99,80</s>
                    </div>
                    <!-- IMAGEM OFERTA DESPERTAR -->
                    <img class="card-img mt-2" src="assets/despertar/melhor_oferta.webp" style="width: 100%; height: 100%">
                    <!-- TEXTO VALORES -->
                    <div class="text-color-black">
                        <div class="f-size-1-5 mt-2">
                            12x de
                        </div>
                        <div class="f-size-6 text-weight-extrabold">
                            R$ 6,97
                        </div>
                        <div class="f-size-1-6">
                            ou <strong>R$69,69</strong> à vista
                        </div>
                    </div>
                    <!-- CALL FOR ACTION -->
                    <div class="row">
                        <div class="col-md-1"></div>
                        <div class="col-md-10">
                            <a href="https://checkout.mycheckout.com.br/checkout/615e2498dd1f702e08653fdc?">
                                <button class="btn btn-lg col-12 mt-4 btn-aprender red text-black letter-space-1-5 lh-1-2 f-size-1-5 text-weight-bold">QUERO APRENDER AGORA</button>
                            </a>
                        </div>
                        <div class="col-md-1"></div>
                    </div>
                    <!-- IMAGEM PAGAMENTOS -->
                    <img class="card-img mt-3" src="assets/libertar/pagamentos.webp" style="width: 100%; height: 100%">
                    <!-- TEXTO SEGURANCA -->
                    <div class="row">
                        <div class="col-md-2"></div>
                        <div class="col-md-8">
                            <div class="text-black mt-3">
                                🔒 Ambiente seguro. Seus dados estão protegidos e sua compra é 100% segura.
                            </div>
                        </div>
                        <div class="col-md-2"></div>
                    </div>
                </div>
            </div>

        </div>
        <div class="col-md-1"></div>
    </div>

</div>