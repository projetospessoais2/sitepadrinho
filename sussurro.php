<?php

$dadosGerais = [
'htmlSubTitle' => 'Já imaginou como seria saber como fazer alguém <span class="text-weight-bold">CHEGAR LÁ</span> só com <span class="text-weight-bold">A SUA VOZ</span>, além de saber usar a <span class="text-weight-bold">Hipnose</span> para <span class="text-weight-bold">Enriquecer, Emagrecer, Ansiedade,</span> e até criar um <span class="text-weight-bold">Script de Vendas?</span>',
'linkCompra' => 'https://checkout.mycheckout.com.br/checkout/61aec1149f4ca90b571f3f50?'
];

$dadosFooter = [
    'isFooterImage' => false,
    'footerClass'   => '',
    'footer'        => '',
    'hasText'       => false,
];

?>

<!DOCTYPE html>
<html lang="pt-BR">

<head>
    <?php require('default/header.php'); ?>
    <title>SUSSURO TÂNTRICO 🔥</title>
    <link rel="stylesheet" href="css/sussurro/sussurro.css">
</head>

<body>
    <?php require('contents/sussurro_content.php'); ?>
    <?php require('contents/about/sussurro_about.php'); ?>
    <?php require('default/lastSectionV2.php'); ?>
    <?php require('default/scriptsBody.php'); ?>
</body>

<?php require('default/footer.php'); ?>

</html>