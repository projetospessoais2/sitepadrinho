<!DOCTYPE html>
<html lang="pt-BR">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="viewport" content="initial-scale=1" />

    <link rel="icon" type="image/x-icon" href="assets/favicon.ico">

    <title>Dicas do Padrinho</title>
    <link href="css/home.css" rel="stylesheet">
</head>

<?php require('contents/index_content.php'); ?>

</html>