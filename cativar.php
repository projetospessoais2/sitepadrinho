<?php
$dadosGerais = [
    'contents/about/cativar_about.php' => [],
    'default/duvidasFrequentes.php' => [
        'textColor'    => 'cativar',
        'neonBehavior' => 'neon-behavior-purple',
        'showButton'   => true,
        'button'  => [
            'class'    => 'btn btn-lg col-10 mt-4 btn-aprender text-color-white letter-space-1-5 lh-1-2 f-size-1-5 text-weight-bold',
            'text'     => 'QUERO APRENDER A CATIVAR',
            'subtitle' => 'por apenas 7x de R$ 6,35'
        ],
        'cols'             => [
            'divider' => 'col-md-3',
            'middle'  => 'col-md-6'
        ]
    ],
];

$dadosFooter = [
    'isFooterImage' => false,
    'footerClass'   => 'neon-white shadow-pink mt-2 titleCativar',
    'footer'        => 'CATIVAR',
    'hasText'       => false,
    'colsFooter'    => [
        'divider' => 'col-md-4',
        'middle'  => 'col-md-4'
    ]
];

?>

<!DOCTYPE html>
<html lang="pt-BR">

<head>
    <?php require('default/header.php'); ?>
    <title>CATIVAR</title>
    <link rel="stylesheet" href="css/cativar/cativar.css">
</head>

<body>
    <?php require('contents/cativar_content.php'); ?>
    <?php require('default/loadLastSection.php'); ?>
    <?php require('default/scriptsBody.php'); ?>
</body>

<?php require('default/footer.php'); ?>

</html>