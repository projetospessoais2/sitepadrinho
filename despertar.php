<?php
$dadosGerais = [
    'default/duvidasFrequentes.php' => [
        'textColor'    => 'despertar',
        'neonBehavior' => 'neon-behavior-yellow',
        'showButton'   => true,
        'button'  => [
            'class'    => 'btn btn-lg col-8 mt-4 btn-aprender-v2 text-color-white letter-space-1-5 lh-1-2 f-size-1-5 text-weight-bold',
            'text'     => 'QUERO APRENDER AGORA',
            'subtitle' => ''
        ],
        'cols'             => [
            'divider' => 'col-md-3',
            'middle'  => 'col-md-6'
        ]
    ],
    'contents/about/despertar_about.php' => [],
];

$dadosFooter = [
    'isFooterImage' => true,
    'footer'        => 'assets/despertar/logo-despertar.webp',
    'hasText'       => false,
    'colsFooter'    => [
        'divider' => 'col-md-5',
        'middle'  => 'col-md-2'
    ]
];

?>

<!DOCTYPE html>
<html lang="pt-BR">
    <head>
        <?php require('default/header.php'); ?>
        <title>DESPERTAR</title>
        <link rel="stylesheet" href="css/despertar/despertar.css"> 
    </head>

    <body>
        <?php require ('contents/despertar_content.php'); ?>
        <?php require ('default/loadLastSection.php'); ?>
        <?php require('default/scriptsBody.php'); ?>
    </body>

    <?php require ('default/footer.php'); ?>
</html>