<?php
$dadosGerais = [
    'contents/about/preliminar_about.php' => [],
    'default/duvidasFrequentes.php' => [
        'textColor'    => 'preliminar',
        'neonBehavior' => 'neon-behavior-red',
        'showButton'   => true,
        'button'  => [
            'class'    => 'btn btn-lg col-8 mt-4 btn-aprender text-color-white letter-space-1-5 lh-1-2 f-size-1-5 text-weight-bold',
            'text'     => 'QUERO APRENDER AGORA',
            'subtitle' => ''
        ],
        'cols'             => [
            'divider' => 'col-md-3',
            'middle'  => 'col-md-6'
        ]
    ],
];

$dadosFooter = [
    'isFooterImage' => true,
    'footer'        => 'assets/preliminar/logo-preliminar.webp',
    'hasText'       => false,
    'colsFooter'    => [
        'divider' => 'col-md-4',
        'middle'  => 'col-md-4'
    ]
];

?>

<!DOCTYPE html>
<html lang="pt-BR">
    <head>
        <?php require('default/header.php'); ?>
        <title>PRELIMINAR</title>
        <link rel="stylesheet" href="css/preliminar/preliminar.css"> 
    </head>

    <body>
        <?php require ('contents/preliminar_content.php'); ?>
        <?php require ('default/loadLastSection.php'); ?>
        <?php require('default/scriptsBody.php'); ?>
    </body>

    <?php require ('default/footer.php'); ?>
</html>