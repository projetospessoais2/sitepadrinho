<?php
$dadosGerais = [
    'default/duvidasFrequentes.php' => [
        'textColor'    => 'libertar',
        'neonBehavior' => 'neon-behavior-orange-intense',
        'showButton'   => true,
        'button'  => [
            'class'    => 'btn btn-lg col-10 mt-4 btn-aprender text-color-white letter-space-1-5 lh-1-2 f-size-1-5 text-weight-bold',
            'text'     => 'QUERO COMPRAR AGORA',
            'subtitle' => ''
        ],
        'cols'             => [
            'divider' => 'col-md-3',
            'middle'  => 'col-md-6'
        ]
    ],
    'contents/about/libertar_about.php' => [],
];

$dadosFooter = [
    'isFooterImage' => true,
    'footerClass'   => '',
    'hasText'       => true,
    'footer'        => 'assets/libertar/libertar_logo_shadow.webp',
    'colsFooter'    => [
        'divider' => 'col-md-4',
        'middle'  => 'col-md-4'
    ]
];

?>

<!DOCTYPE html>
<html lang="pt-BR">

<head>
    <?php require('default/header.php'); ?>
    <title>LIBERTAR</title>
    <link rel="stylesheet" href="css/libertar/libertar.css">
</head>

<body>
    <?php require('contents/libertar_content.php'); ?>
    <?php require('default/loadLastSection.php'); ?>
    <?php require('default/scriptsBody.php'); ?>
</body>

<?php require('default/footer.php'); ?>

</html>